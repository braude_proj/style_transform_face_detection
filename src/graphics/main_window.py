import os
import sys
import threading
import tkinter
import traceback
import webbrowser
from functools import partial
from io import BytesIO
from time import sleep, time
from tkinter import filedialog
from typing import Optional

from PIL import Image as Im
from pysftp import Connection

# Adding project to system path to be able to import other files from project
path = '/'.join(os.path.dirname(os.path.realpath(__file__)).split('/')[0:-2])
if path not in sys.path:
    sys.path.insert(0, path)

from src import utils
import src.face_detection.run_face_detection as face
import src.graphics.settings_window as set_win
import src.graphics.style_bank_window as style_win
from src.face_detection.shared_variables_face_detection import EDGE_LENGTH_X, EDGE_LENGTH_Y


class MainWindowGui:
    sftp: Optional[Connection]
    WINDOW_WIDTH = 900
    WINDOW_HEIGHT = 500

    CANVAS_IMAGE_WIDTH = utils.IMAGE_MIN_SIZE
    CANVAS_IMAGE_HEIGHT = utils.IMAGE_MIN_SIZE

    IMG_FRAME_HEIGHT = 450

    PROGRESS_BAR_AUTO_STEP = 10

    MAX_TIME_UNTIL_KILL = 90
    SLEEP_TIME_FIRST_UPDATE = 3

    content_image_path = utils.CONTENTS_DIR + "Omri.jpg"
    style_image_path = utils.STYLES_DIR + "pre_trained/scream.jpg"

    progress_bar_title = "Progress bar"
    progress_value = 0
    exec_output = None
    cur_path = ""
    cmd = ""
    last_dir_index = 0
    faces = None
    face_semaphore = threading.Semaphore()

    def __init__(self):
        image_frame_list = list()
        content_image_items_list = list()
        style_image_items_list = list()
        result_image_items_list = list()

        self.top = tkinter.Tk()
        self.top.title("Style Transformation Face Detection")

        # ############## Progress bar ##############
        # The progress bar has two functions in this class related to it:
        # 1. Initialize (length and title)
        # 2. Increment the value of it to show the progress

        # Progress bar frame
        self.progress_frame = tkinter.Frame(self.top)
        # Text into the progressbar
        self.text_in_progress_bar = tkinter.ttk.Style(self.progress_frame)
        self.text_in_progress_bar.layout("LabeledProgressbar",
                                         [('LabeledProgressbar.trough',
                                           {'children': [('LabeledProgressbar.pbar',
                                                          {'side': 'left', 'sticky': 'ns'}),
                                                         ("LabeledProgressbar.label",
                                                          {"sticky": ""})],
                                            'sticky': 'nswe'})])
        self.text_in_progress_bar.configure("LabeledProgressbar")
        # The progress bar
        self.progress_bar = tkinter.ttk.Progressbar(self.progress_frame, length=int(self.WINDOW_WIDTH * 3 / 4),
                                                    orient="horizontal", mode="determinate", style='LabeledProgressbar')
        # Progress bar title
        self.progress_title = tkinter.Label(self.progress_frame, text=self.progress_bar_title,
                                            padx=int((self.WINDOW_WIDTH / 4 - len(self.progress_bar_title)) / 2))

        # ############## Menu options ##############
        # Menu options frame:
        menu_options_frame = tkinter.Frame(self.top, width=self.WINDOW_WIDTH, height=50)
        # Setting button:
        setting_icon = utils.path_to_resized_tk_image(utils.SETTING_ICON_PATH, 40)
        setting_button = tkinter.Button(menu_options_frame, text="Settings", image=setting_icon,
                                        command=set_win.SettingsWindowGui)
        setting_button.image = setting_icon
        # Help button:
        help_icon = utils.path_to_resized_tk_image(utils.HELP_ICON_PATH, 40)
        help_button = tkinter.Button(menu_options_frame, text="Help", image=help_icon, command=self.open_help_window)
        help_button.image = help_icon
        # Algorithm selection combobox:
        select_alg_label = tkinter.Label(menu_options_frame, text="Select style transfer algorithm:")
        self.algorithm_options_combobox = tkinter.ttk.Combobox(
            menu_options_frame, width=200, values=["NAAS", "Style Only", "Fast Transformation"], state='readonly')
        self.algorithm_options_combobox.current(utils.alg_index)
        self.algorithm_options_combobox.bind("<<ComboboxSelected>>", self.alg_combobox_clicked)

        # ############## Content image ##############
        # Contains the input image of the content along with it related buttons

        # Content image frame:
        self.content_image_frame = tkinter.Frame(self.top, height=self.IMG_FRAME_HEIGHT, width=self.CANVAS_IMAGE_WIDTH)
        image_frame_list.append(self.content_image_frame)
        # Content image canvas:
        # The canvas will hold the image of the content image
        self.content_canvas_image = tkinter.Canvas(self.content_image_frame)
        self.content_canvas_image.image = utils.path_to_resized_tk_image(self.content_image_path, face_image=True)
        self.content_canvas_image.create_image(self.CANVAS_IMAGE_WIDTH / 2 + 10, self.CANVAS_IMAGE_HEIGHT / 2 + 10,
                                               image=self.content_canvas_image.image)
        content_image_items_list.append(self.content_canvas_image)
        # Content image title
        self.content_title = tkinter.Label(self.content_image_frame, text="Content image")
        content_image_items_list.append(self.content_title)
        # Content image selection button
        select_content_image_button = partial(self.select_image_button, image_type="content")
        self.select_content_image_button = tkinter.Button(self.content_image_frame, text="Select image",
                                                          command=select_content_image_button)
        content_image_items_list.append(self.select_content_image_button)
        self.detect_face_button = tkinter.Button(self.content_image_frame, text="Detect faces",
                                                 command=self.pre_faces_detection)
        content_image_items_list.append(self.detect_face_button)

        # ############## Style image frame ##############
        # Contains the input image of the style along with it related buttons

        # Style image frame:
        self.style_image_frame = tkinter.Frame(self.top, height=self.IMG_FRAME_HEIGHT, width=self.CANVAS_IMAGE_WIDTH)
        image_frame_list.append(self.style_image_frame)
        # Style image canvas:
        # The canvas will hold the image of the style image
        self.style_canvas_image = tkinter.Canvas(self.style_image_frame)
        self.style_canvas_image.image = utils.path_to_resized_tk_image(self.style_image_path)
        self.style_canvas_image.create_image(self.CANVAS_IMAGE_WIDTH / 2 + 10, self.CANVAS_IMAGE_HEIGHT / 2 + 10,
                                             image=self.style_canvas_image.image)
        style_image_items_list.append(self.style_canvas_image)
        # Style image title
        self.styleTitle = tkinter.Label(self.style_image_frame, text="Style image")
        style_image_items_list.append(self.styleTitle)
        # Style image selection button
        select_style_image_button = partial(self.select_image_button, image_type="style")
        self.select_style_image_button = tkinter.Button(self.style_image_frame, text="Select image",
                                                        command=select_style_image_button)
        style_image_items_list.append(self.select_style_image_button)
        self.select_bank_style_image_button = tkinter.Button(self.style_image_frame, text="Select from bank",
                                                             command=self.select_style_from_bank)
        style_image_items_list.append(self.select_bank_style_image_button)

        # ############## Apply button frame ##############

        # Apply style frame:
        self.apply_frame = tkinter.Frame(self.top, height=self.IMG_FRAME_HEIGHT, width=100)
        image_frame_list.append(self.apply_frame)
        # Apply style button:
        self.apply_button = tkinter.Button(self.apply_frame, text="Apply\nstyle",
                                           command=self.apply_style_button_clicked)

        # ############## Result image frame ##############
        # Contains the result image of the style along with it related radio buttons

        # Result frame:
        self.result_frame = tkinter.Frame(self.top, height=self.IMG_FRAME_HEIGHT, width=self.CANVAS_IMAGE_WIDTH)
        image_frame_list.append(self.result_frame)
        # The canvas will hold the image view of the result image
        self.result_image_canvas = tkinter.Canvas(self.result_frame)
        self.result_image = utils.path_to_resized_tk_image(utils.DEFAULT_RESULT_PATH)
        self.result_image_canvas.image = self.result_image
        self.result_image_canvas.create_image(self.CANVAS_IMAGE_WIDTH / 2 + 10,
                                              self.CANVAS_IMAGE_HEIGHT / 2 + 10, image=self.result_image_canvas.image)
        result_image_items_list.append(self.result_image_canvas)
        # Initialize the current_result_image, needed for update the result
        self.current_result_image = Im.open(self.style_image_path)
        # Title for result image
        self.result_title = tkinter.Label(self.result_frame, text="Result image")
        result_image_items_list.append(self.result_title)
        # Radio buttons - where the style transfer will affect
        # # TODO: Add functionality to the radio buttons
        # self.setting_sort = tkinter.StringVar()
        # self.face_filter_button = tkinter.Radiobutton(self.result_frame, text="Face filter",
        #                                               variable=self.setting_sort, value="alpha")
        # self.face_filter_button.select()
        # result_image_items_list.append(self.face_filter_button)
        # self.background_filter_button = tkinter.Radiobutton(self.result_frame, text="Background filter",
        #                                                     variable=self.setting_sort, value="beta")
        # result_image_items_list.append(self.background_filter_button)

        self.progress_frame.pack(side=tkinter.BOTTOM, fill='x')
        self.progress_bar.pack(side=tkinter.RIGHT)
        self.progress_title.pack(side=tkinter.LEFT, anchor='center')
        menu_options_frame.pack(expand=True, fill="x", side=tkinter.TOP)
        menu_options_frame.pack_propagate(0)
        setting_button.pack(expand=True, anchor='n', padx=5, side=tkinter.LEFT)
        help_button.pack(expand=True, anchor='n', padx=5, side=tkinter.LEFT)
        select_alg_label.pack(expand=True, anchor='n', padx=5, side=tkinter.LEFT)
        self.algorithm_options_combobox.pack(expand=True, anchor='n', padx=5, side=tkinter.LEFT)
        self.apply_button.pack(fill='x', anchor='e', expand=True)

        for frame in image_frame_list:
            frame.pack(expand=True, fill='x', side=tkinter.LEFT)
            frame.pack_propagate(0)

        anchor_pos = ('n', 'e', 's')
        images_list = [content_image_items_list, style_image_items_list, result_image_items_list]
        for image_list in images_list:
            i = 0
            for item in image_list:
                item.pack(fill='x', anchor=anchor_pos[(i % 3)], expand=True)
                i += 1
        return

    @staticmethod
    def open_help_window():
        """
        Go to our GIT web page
        """
        webbrowser.open('https://bitbucket.org/braude_proj/style_transform_face_detection/src/master/')
        return

    def progress_start(self):
        """
        Set initial values to the progress bar
        """
        self.progress_bar_title = "Progress bar"
        self.progress_title.config(text="Progress bar",
                                   padx=int((self.WINDOW_WIDTH / 4 - len(self.progress_bar_title)) / 2))
        self.progress_bar.config(maximum=utils.DEFAULT_TRANSFER_ITERATIONS)
        return

    def progress_step(self, description, step_size=0):
        """
        Increase the value in the progress bar
        :param step_size: The size of the step that the progress bar need to increase
        :param description: Text in the progress bar (explain what progress was made)
        """
        self.text_in_progress_bar.configure("LabeledProgressbar", text=description)
        self.progress_bar.step(step_size)
        return

    def update_gui_img(self, canvas_image, path_to_img, type_to_be_changed):
        selected_img = utils.path_to_resized_tk_image(path_to_img)
        canvas_image.image = selected_img
        canvas_image.create_image(self.CANVAS_IMAGE_WIDTH / 2 + 10, self.CANVAS_IMAGE_HEIGHT / 2 + 10,
                                  image=canvas_image.image)
        print(type_to_be_changed + "path: " + self.content_image_path)
        return

    def alg_combobox_clicked(self, event):
        """
        Set the selected alg as the alg that should operate
        """
        selected_ago = self.algorithm_options_combobox.get()
        if selected_ago == "NAAS":
            utils.alg_index = utils.NAAS_ALG
        elif selected_ago == "Style Only":
            utils.alg_index = utils.STYLE_ONLY
        elif selected_ago == "Fast Transformation":
            utils.alg_index = utils.PRE_TRAINED_NAAS
            if utils.fast_trans_image_name is None:
                self.select_style_from_bank()
        return

    def select_image_button(self, image_type="content"):
        """
        Open select image window and present the selected image
        :param image_type: content or style, to know where to present the selected image
        """
        path_to_selected_image = filedialog.askopenfilename(
            initialdir=utils.IMAGES_DIR, title="Select " + image_type + " image",
            filetypes=(("JPG files", "*.jpg"), ("PNG files", "*.png"), ("GIF files", "*.gif")))
        # Check if image was selected
        if not isinstance(path_to_selected_image, str) or \
                path_to_selected_image == '' or \
                not os.path.isfile(path_to_selected_image):
            return

        content_image = True
        canvas_image = self.content_canvas_image
        if image_type == "content":
            canvas_image = self.content_canvas_image
            self.content_image_path = path_to_selected_image
            print("content path updated: " + self.content_image_path)
        elif image_type == "style":
            content_image = False
            canvas_image = self.style_canvas_image
            self.style_image_path = path_to_selected_image
            if utils.alg_index == utils.PRE_TRAINED_NAAS:
                utils.popup_msg("INFO", "Algorithm changed from fast style transfer to NAAS.\n"
                                        "This is because fast style transfer worked only with pre trained networks "
                                        "and cannot use any style image instantly.")
                utils.alg_index = utils.NAAS_ALG
                self.algorithm_options_combobox.current(utils.NAAS_ALG)
        # Update image to the window
        canvas_image.image = utils.path_to_resized_tk_image(path_to_selected_image, face_image=content_image)
        canvas_image.create_image(self.CANVAS_IMAGE_WIDTH / 2 + 10, self.CANVAS_IMAGE_HEIGHT / 2 + 10,
                                  image=canvas_image.image)
        return

    def pre_faces_detection(self):
        """
        Open and resize the image to size of (EDGE_LENGTH_X x EDGE_LENGTH_Y) and after the detection resize to size of
        (utils.IMAGE_MIN_SIZE, utils.IMAGE_MIN_SIZE)
        :return: TODO: Fill that
        """
        try:
            print("STARTING FACE DETECTION! -> " + self.content_image_path)
            img = Im.open(self.content_image_path)
            image_for_detection = utils.resize_with_pad_keep_ratio(img, EDGE_LENGTH_X, EDGE_LENGTH_Y)
            self.faces = face.detect_faces(image_for_detection, self.content_image_path.split("/")[-1].split(".")[0])

            # Present the pooints that define the rectangle
            # faces_in_image = [item[0] for item in self.faces]
            # for points in faces_in_image:
            #     point1 = utils.new_position_after_resize((points[0]),
            #                                            image_for_detection.size[1], image_for_detection.size[0],
            #                                            utils.IMAGE_MIN_SIZE, utils.IMAGE_MIN_SIZE)
            #     point2 = utils.new_position_after_resize((points[1]),
            #                                            image_for_detection.size[1], image_for_detection.size[0],
            #                                               utils.IMAGE_MIN_SIZE, utils.IMAGE_MIN_SIZE)
            #
            #     point3 = utils.new_position_after_resize((points[2]),
            #                                            image_for_detection.size[1], image_for_detection.size[0],
            #                                            utils.IMAGE_MIN_SIZE, utils.IMAGE_MIN_SIZE)
            #     point4 = utils.new_position_after_resize((points[3]),
            #                                            image_for_detection.size[1], image_for_detection.size[0],
            #                                            utils.IMAGE_MIN_SIZE, utils.IMAGE_MIN_SIZE)
            #
            #     utils.present_coordinates([point1, point2, point3, point4], "Result eyes- after resize",
            #                               np.asarray(image_for_detection.resize
            #                                          ((utils.IMAGE_MIN_SIZE, utils.IMAGE_MIN_SIZE))))
        finally:
            print("face detection completed releasing (face_semaphore.release())")
            self.face_semaphore.release()
        return

    @staticmethod
    def select_style_from_bank():
        """
        Start the thread of Style bank window
        """
        execution = threading.Thread(target=style_win.StyleBankWindow)
        execution.start()
        return

    def apply_style_button_clicked(self):
        """
        Start the thread of remote_style_transfer
        """
        style_thread = threading.Thread(target=self.remote_style_transfer)
        face_detect_thread = threading.Thread(target=self.pre_faces_detection)
        print("Apply button clicked: Locking face semaphore (face_semaphore.acquire())")
        self.face_semaphore.acquire()
        face_detect_thread.start()
        style_thread.start()
        return

    def remote_style_transfer(self):
        """
        Gather all configurations made by user and run style transfer algorithm using these configs
        """
        # TODO: Don't refresh the result canvas all the time, just when new result arrives
        self.progress_start()
        self.progress_step("Connecting to server...")
        self.sftp = utils.connect_to_sftp_server(host=utils.HOST, port=utils.REMOTE_PORT, username=utils.REMOTE_USER,
                                                 password=utils.REMOTE_USER_PASSWORD,
                                                 private_key=utils.REMOTE_PRIVATE_KEY_PATH,
                                                 private_key_pass=utils.REMOTE_PRIVATE_KEY_PASSWORD)
        if self.sftp is None:
            utils.popup_msg("ERROR", "Style Transfer Failed!\n"
                                     "See log for more info")
            return
        directories = self.sftp.listdir()
        directories = list(map(int, directories))
        directories.sort()
        if not directories:
            self.last_dir_index = 0
        else:
            self.last_dir_index = int(directories[-1]) + 1
        self.sftp.mkdir(str(self.last_dir_index))
        os.mkdir(utils.RESULTS_DIR + str(self.last_dir_index))
        self.sftp.chdir(str(self.last_dir_index))
        self.sftp.mkdir("progress")
        content_image = Im.open(self.content_image_path)
        content_image = utils.resize_with_pad_keep_ratio(content_image, self.CANVAS_IMAGE_WIDTH, self.CANVAS_IMAGE_HEIGHT)
        # content_image.resize((self.CANVAS_IMAGE_WIDTH, self.CANVAS_IMAGE_HEIGHT), resample=Im.BILINEAR)
        saved_content_path = utils.IMAGES_DIR + "resized_content_thumb.jpg"
        content_image.save(saved_content_path, "PNG")
        # self.content_image_path = utils.IMAGES_DIR + "resized_content_thumb.jpg"
        style_image = Im.open(self.style_image_path)
        style_image = utils.resize_with_pad_keep_ratio(style_image, self.CANVAS_IMAGE_WIDTH, self.CANVAS_IMAGE_HEIGHT)
        saved_style_path = utils.IMAGES_DIR + "resized_style_thumb.jpg"
        style_image.save(saved_style_path, "PNG")
        # self.style_image_path = utils.IMAGES_DIR + "resized_style_thumb.jpg"

        self.progress_step("Uploading content image...")
        self.sftp.put(saved_content_path)  # Upload content photo to public/# on remote
        self.progress_step("Uploading style image...")
        self.sftp.put(saved_style_path)  # Upload style photo to public/# on remote
        self.cur_path = self.sftp.pwd + '/'
        self.progress_step("Running style transfer on server...")
        self.cmd = \
            "python3.6 " + \
            utils.REMOTE_SERVER_COMMAND_PATH + " " +\
            "-content_path " + \
            self.cur_path + saved_content_path.split('/')[-1].split('\\')[-1] + " "\
            "-style_path " + \
            self.cur_path + saved_style_path.split('/')[-1].split('\\')[-1] + " " +\
            "-output_path " + \
            self.cur_path + " " +\
            "-max_iter " + str(utils.DEFAULT_TRANSFER_ITERATIONS) + " "

        def switch_alg(x):
            return {
                0: "-alg naas ",
                1: "-alg style_only ",
                2: "-alg fast_trans -fast_trans_style " + utils.fast_trans_image_name + " ",
            }[x]
        self.cmd += switch_alg(utils.alg_index)
        if utils.SHOW_PROGRESS:
            self.cmd += "-show_progress " + str(utils.DEFAULT_PROGRESS_UPDATE_INTERVAL) + " "
        self.cmd += "> " + self.cur_path + "/logfile.txt"
        print("EXECUTING: \n" + "\n".join(self.cmd.split(" ")))
        update_progress = threading.Thread(target=self.update_progress_in_result_image)
        execution = threading.Thread(target=self.remote_command)
        execution.start()
        update_progress.start()
        return

    def update_progress_in_result_image(self):
        """
        Update the result image to the last result got form the server and present the progress in the progress bar
        """
        sleep(self.SLEEP_TIME_FIRST_UPDATE)
        last_progress_val = 0
        dir_path = "progress"
        start_time = time()
        updated = {}
        sftp = utils.connect_to_sftp_server()
        if sftp is None:
            utils.popup_msg("ERROR", "Result Image Progress Failed!\n"
                                     "could not connect to server!\n"
                                     "See log for more info")
            return
        sftp.chdir(self.cur_path)
        while True:
            dir_files = sftp.listdir(dir_path)
            progress_files = list(map(int, dir_files))
            progress_files.sort()
            if not progress_files:
                sleep(1)
                if time() - start_time > self.MAX_TIME_UNTIL_KILL:
                    break
                continue
            latest_image_path = str(progress_files[-1])
            if latest_image_path in updated.keys():
                sleep(1)
                if time() - start_time > self.MAX_TIME_UNTIL_KILL:
                    break
                continue
            start_time = time()
            updated[latest_image_path] = 1
            flo = BytesIO()
            # noinspection PyBroadException
            try:
                sftp.getfo(dir_path + "/" + latest_image_path, flo)
            except Exception:
                # Many different exceptions can be raised by this function
                # therefore we want to catch any and print it out to the user
                print("Unexpected error:" + str(sys.exc_info()[0]))
                break
            if time() - start_time > self.MAX_TIME_UNTIL_KILL:
                break
            flo.seek(0)
            progress_value = latest_image_path.split('/')[-1]
            if progress_value is '':
                progress_value = latest_image_path.split('\\')[-1]
            if int(progress_value) >= utils.DEFAULT_TRANSFER_ITERATIONS:
                self.progress_step("Finalizing...")
                self.progress_bar.config(value=utils.DEFAULT_TRANSFER_ITERATIONS)
                break
            self.progress_step("Completed " + progress_value + " / " + str(utils.DEFAULT_TRANSFER_ITERATIONS),
                               int(progress_value) - last_progress_val)
            last_progress_val = int(progress_value)
            best_image = Im.open(flo)
            if self.current_result_image.size != best_image.size:
                self.result_image_canvas.delete("all")
                self.current_result_image = best_image
                self.result_image_canvas.image = utils.pil_image_to_resized_tk_image(best_image)
                self.result_image_canvas.create_image(self.CANVAS_IMAGE_WIDTH / 2 + 10,
                                                      self.CANVAS_IMAGE_HEIGHT / 2 + 10,
                                                      image=self.result_image_canvas.image)
                self.result_image_canvas.update()
            else:
                alpha = 0
                while 1.0 > alpha:
                    self.current_result_image = Im.blend(self.current_result_image, best_image, alpha)
                    alpha = alpha + 0.01
                    self.result_image_canvas.image = utils.pil_image_to_resized_tk_image(self.current_result_image)
                    self.result_image_canvas.create_image(self.CANVAS_IMAGE_WIDTH / 2 + 10,
                                                          self.CANVAS_IMAGE_HEIGHT / 2 + 10,
                                                          image=self.result_image_canvas.image)
                    self.result_image_canvas.update()
                    sleep(0.1)
            sleep(1)
        sftp.close()
        return

    def remote_command(self):
        """
        TODO: Fill that
        :return:
        """
        self.exec_output = self.sftp.execute(self.cmd)
        for line in self.exec_output:
            sys.stdout.write(line.decode("ascii"))
        print("downloading log file from server")
        latest_result_log_path = utils.RESULTS_DIR + '/' + "latest_remote_log.txt"
        self.sftp.get(self.cur_path + "/logfile.txt", latest_result_log_path)
        try:
            local_path = str(utils.RESULTS_DIR + r'/' + r"latest_result.jpg")
            remote_path = str(self.cur_path + 'result.jpg')
            self.progress_step("Downloading result from server...")
            self.sftp.get(remotepath=remote_path, localpath=local_path)         # get a remote file
            self.progress_step("Download complete")
        except IOError:
            self.progress_step("FAILED TO CREATE TRANSFORMATION IMAGE")
            if os.path.isfile(latest_result_log_path):
                print("REMOTE SERVER LOG FILE!\n-----------------------------------------------")
                print("Reading from local file: " + latest_result_log_path)
                log_file = open(latest_result_log_path, "r", encoding="utf8")
                print(str(log_file.read()))
                print("REMOTE SERVER LOG FILE END!\n-----------------------------------------------")
            print("REMOTE SERVER ERROR!\n"
                  "-----------------------------------------------")
            for line in self.exec_output:
                sys.stdout.write('\x1b[1;31m' + line.decode("ascii") + '\x1b[0m')
            print("REMOTE SERVER ERROR END!\n"
                  "-----------------------------------------------")
            self.sftp.close()
            return
        try:
            self.progress_step("Downloading result directory from server...")
            utils.get_r_portable(self.sftp, remote_dir=str(self.cur_path),
                                 local_dir=str(utils.RESULTS_DIR) + str(self.last_dir_index))
            self.progress_step("Download complete")
        except IOError:
            self.progress_step("IOError: FAILED TO DOWNLOAD DIR TREE")
            print(traceback.format_exc())
        if os.path.isfile(latest_result_log_path):
            print("REMOTE SERVER LOG FILE!\n-----------------------------------------------")
            print("Reading from local file: " + latest_result_log_path)
            log_file = open(latest_result_log_path, "r", encoding="utf8")
            print(str(log_file.read()))
            print("REMOTE SERVER LOG FILE END!\n-----------------------------------------------")
        self.sftp.close()
        print("result image waiting for faces (face_semaphore.acquire())")
        self.face_semaphore.acquire()
        # self.faces = face.detect_faces(Im.open(self.saved_content_path))
        result_image = Im.open(local_path)
        content_image = utils.resize_with_pad_keep_ratio(Im.open(self.content_image_path), utils.IMAGE_MIN_SIZE, utils.IMAGE_MIN_SIZE)
        output_image = utils.filter_faces_from_result(result_image, content_image, self.faces)
        self.faces = None
        self.result_image_canvas.image = utils.pil_image_to_resized_tk_image(output_image)
        self.result_image_canvas.create_image(self.CANVAS_IMAGE_WIDTH / 2 + 10,
                                              self.CANVAS_IMAGE_HEIGHT / 2 + 10, image=self.result_image_canvas.image)
        print("result image done (face_semaphore.release())")
        self.face_semaphore.release()
        return
