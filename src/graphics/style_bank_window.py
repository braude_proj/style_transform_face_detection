import os
import sys
from functools import partial
from tkinter import Tk, Button

# Adding project to system path to be able to import other files from project
path = '/'.join(os.path.dirname(os.path.realpath(__file__)).split('/')[0:-2])
if path not in sys.path:
    sys.path.insert(0, path)

import src.utils as utils

rows = 2
columns = 3


class StyleBankWindow:
    canvas_image_width = 250
    style_images_path = utils.STYLES_DIR + "pre_trained/"
    style_images_paths = []
    style_tk_images = []
    ref_labels = []

    def __init__(self):
        self.top = Tk()
        self.top.title("Style Bank")
        self.buttons = []
        self.style_images_paths = [os.path.join(self.style_images_path, f)
                                   for f in os.listdir(self.style_images_path)
                                   if os.path.isfile(os.path.join(self.style_images_path, f))]
        self.style_images_names = [f.split('.')[0]
                                   for f in os.listdir(self.style_images_path)
                                   if os.path.isfile(os.path.join(self.style_images_path, f))]
        for i in range(rows):
            for j in range(columns):
                index = i*columns+j
                if index >= len(self.style_images_paths):
                    break
                func = partial(self.set_path_for_fast_trans, index)
                self.buttons.insert(0, Button(self.top, text=self.style_images_names[index], borderwidth=5))
                self.buttons[0].bind("<Button-1>", func)
                self.buttons[0].grid(row=i, column=j)

        self.top.mainloop()
        return

    def set_path_for_fast_trans(self, index, press):
        """
        TODO: Fill that
        :param index:
        :param press:
        :return:
        """
        print("style image name for fast trans was updated to " + str(self.style_images_names[index])
              + " press = " + str(press))
        utils.fast_trans_image_name = self.style_images_names[index]
        utils.alg_index = 2  # second index is fast trans algorithm
        utils.SHOW_PROGRESS = 0
        utils.MAIN_WINDOW.style_image_path = self.style_images_paths[index]
        utils.MAIN_WINDOW.update_gui_img(utils.MAIN_WINDOW.style_canvas_image, self.style_images_paths[index], "style")
        self.top.destroy()
        return
