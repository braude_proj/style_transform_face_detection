import tkinter

import src.utils as utils


class SettingsWindowGui:
    window_width = 920
    window_height = 350
    user_name = utils.REMOTE_USER
    port = utils.REMOTE_PORT
    user_password = ""
    host = utils.HOST
    private_key_path = utils.REMOTE_PRIVATE_KEY_PATH
    private_key_password = utils.REMOTE_PRIVATE_KEY_PASSWORD
    remote_host_text: tkinter.Entry

    def __init__(self):
        self.settings = tkinter.Tk()
        self.settings.title("Settings")
        # Root frame
        title_font = 'Helvetica 12 bold'
        root_frame = tkinter.Frame(self.settings, width=self.window_width, height=self.window_height, relief='raised',
                                   borderwidth=5)
        # ############## REMOTE GPU FRAME ##############
        # This frame contains the information regarding cloud computing or remote gpu to run the transfer faster
        remote_gpu_frame = tkinter.Frame(root_frame, width=self.window_width / 3 - 120, borderwidth=5,
                                         highlightbackground="red", highlightcolor="red", highlightthickness=1, bd=0)
        title_label_remote_gpu = tkinter.Label(remote_gpu_frame, text="Remote GPU Options:", font=title_font)
        host_address_label = tkinter.Label(remote_gpu_frame, text="Remote host address:")
        # Text fields to fill remote host information
        self.h = tkinter.StringVar(remote_gpu_frame, value=utils.HOST)
        self.remote_host_text = tkinter.Entry(remote_gpu_frame, textvariable=self.h)

        host_port_label = tkinter.Label(remote_gpu_frame, text="Remote port:")
        # Text fields to fill remote host information
        self.po = tkinter.StringVar(remote_gpu_frame, value=utils.REMOTE_PORT)
        self.remote_port_text = tkinter.Entry(remote_gpu_frame, textvariable=self.po)

        host_user_name_label = tkinter.Label(remote_gpu_frame, text="Username:")
        # Text fields to fill remote user information
        self.un = tkinter.StringVar(remote_gpu_frame, value=utils.REMOTE_USER)
        self.remote_user_name_text = tkinter.Entry(remote_gpu_frame, textvariable=self.un)

        host_user_password_label = tkinter.Label(remote_gpu_frame, text="User password:")
        # Text fields to fill remote key and password information
        self.up = tkinter.StringVar(remote_gpu_frame, value=utils.REMOTE_USER_PASSWORD)
        self.remote_user_password_text = tkinter.Entry(remote_gpu_frame, textvariable=self.up)

        host_private_key_path_label = tkinter.Label(remote_gpu_frame, text="Private key path:")
        # Text fields to fill remote key and password information
        self.private_key_path = tkinter.StringVar(remote_gpu_frame, value=utils.REMOTE_PRIVATE_KEY_PATH)
        self.remote_private_key_path_text = tkinter.Entry(remote_gpu_frame, textvariable=self.private_key_path)

        host_private_key_paraphrase_label = tkinter.Label(remote_gpu_frame, text="Private key paraphrase:")
        # Text fields to fill remote key and password information
        self.private_key_pass = tkinter.StringVar(remote_gpu_frame, value=utils.REMOTE_PRIVATE_KEY_PASSWORD)
        self.remote_private_key_paraphrase_text = tkinter.Entry(remote_gpu_frame, textvariable=self.private_key_pass)

        test_connection_button = tkinter.Button(remote_gpu_frame, text="Test connection",
                                                command=self.test_and_update_remote_info)
        # ############## Face Detection DEBUG Options FRAME ##############
        # This frame contains the information regarding cloud computing or remote gpu to run the transfer faster
        debug_opt_frame = tkinter.Frame(root_frame,  width=(2*self.window_width)/3 - 120, borderwidth=5)
        face_detection_checkboxes_frame = tkinter.Frame(debug_opt_frame, width=self.window_width / 3 - 60,
                                                        borderwidth=5, highlightbackground="red", highlightcolor="red",
                                                        highlightthickness=1, bd=0)
        title_label_fd = tkinter.Label(face_detection_checkboxes_frame, text="Face Detection Options:", font=title_font)
        face_detection_debug_values = [utils.DEBUG_SHOW_CLOSED_IMAGE_MODE,
                                       utils.DEBUG_SHOW_VALLY_FIELD_MODE,
                                       utils.DEBUG_SHOW_ALL_POSSIBLE_EYES_MODE,
                                       utils.DEBUG_SHOW_BEST_POSSIBLE_EYES_MODE,
                                       utils.DEBUG_SHOW_AFTER_ROTATE_MODE,
                                       utils.DEBUG_SHOW_FINAL_RESULTS,
                                       utils.DEBUG_INFO_MODE,
                                       ]

        self.int_vars_debug_opts = \
            [tkinter.IntVar(face_detection_checkboxes_frame, value=x) for x in face_detection_debug_values]
        show_closed_image_checkbutton = tkinter.Checkbutton(
            face_detection_checkboxes_frame, text="Show closed image", variable=self.int_vars_debug_opts[0])
        show_valley_field_checkbutton = tkinter.Checkbutton(
            face_detection_checkboxes_frame, text="Show valley field", variable=self.int_vars_debug_opts[1])
        show_all_pos_eyes_checkbutton = tkinter.Checkbutton(
            face_detection_checkboxes_frame, text="Show all possible eyes", variable=self.int_vars_debug_opts[2])
        show_pos_eyes_checkbutton = tkinter.Checkbutton(
            face_detection_checkboxes_frame, text="Show best possible eyes", variable=self.int_vars_debug_opts[3])
        after_rotation_checkbutton = tkinter.Checkbutton(
            face_detection_checkboxes_frame, text="After rotation index", variable=self.int_vars_debug_opts[4])
        show_final_checkbutton = tkinter.Checkbutton(
            face_detection_checkboxes_frame, text="Show final results", variable=self.int_vars_debug_opts[5])
        show_info_checkbutton = tkinter.Checkbutton(
            face_detection_checkboxes_frame, text="Show info", variable=self.int_vars_debug_opts[6])

        # ############## Style transfer DEBUG Options FRAME ##############
        # This frame contains the information regarding cloud computing or remote gpu to run the transfer faster
        style_trans_options_frame = tkinter.Frame(debug_opt_frame, width=self.window_width / 3 - 60, borderwidth=5,
                                                  highlightbackground="red", highlightcolor="red", highlightthickness=1,
                                                  bd=0)
        title_style_label = tkinter.Label(style_trans_options_frame, text="Style Transform Options:", font=title_font)

        iterations_frame = tkinter.Frame(style_trans_options_frame, width=self.window_width / 3 - 60, borderwidth=5)
        num_of_iter_label = tkinter.Label(iterations_frame, text="Number of Iterations:")
        self.max_inter = tkinter.StringVar(iterations_frame, value=str(utils.DEFAULT_TRANSFER_ITERATIONS))
        max_iter_entry = tkinter.Entry(iterations_frame, textvariable=self.max_inter)

        interval_frame = tkinter.Frame(style_trans_options_frame, width=self.window_width / 3 - 60, borderwidth=5)
        self.show_process = tkinter.IntVar(interval_frame, value=utils.SHOW_PROGRESS)
        show_process_of_tran_checkbutton = tkinter.Checkbutton(
            interval_frame, text="Show Progress of Transformation", variable=self.show_process,
            command=self.toggle_show_progress)
        progress_update_label = tkinter.Label(interval_frame, text="Progress Update Interval:")
        self.process_update_string_var = tkinter.StringVar(
            interval_frame, value=str(utils.DEFAULT_PROGRESS_UPDATE_INTERVAL))
        self.progress_update_inter_entry = tkinter.Entry(interval_frame, textvariable=self.process_update_string_var)

        save_button = tkinter.Button(root_frame, text="Save & Close", command=self.save_and_close)

        root_frame.pack(expand=True, fill="both")
        root_frame.pack_propagate(0)

        title_label_remote_gpu.pack(expand=True, anchor='n', fill="x")
        remote_gpu_frame.pack(expand=True, fill="both", side=tkinter.LEFT)
        remote_gpu_frame.pack_propagate(0)
        host_address_label.pack(anchor='n', fill='x')
        self.remote_host_text.pack(anchor='n')
        host_port_label.pack(anchor='n', fill='x')
        self.remote_port_text.pack(anchor='n')
        host_user_name_label.pack(anchor='n', fill='x')
        self.remote_user_name_text.pack(anchor='n')
        host_user_password_label.pack(anchor='n', fill='x')
        self.remote_user_password_text.pack(anchor='n')
        host_private_key_path_label.pack(anchor='n', fill='x')
        self.remote_private_key_path_text.pack(anchor='n')
        host_private_key_paraphrase_label.pack(anchor='n', fill='x')
        self.remote_private_key_paraphrase_text.pack(anchor='n')
        test_connection_button.pack(anchor='n', pady=15)

        debug_opt_frame.pack(expand=True, fill="both", side=tkinter.LEFT)
        face_detection_checkboxes_frame.pack(expand=True, fill="both", side=tkinter.LEFT)
        face_detection_checkboxes_frame.pack_propagate(0)
        title_label_fd.pack(expand=True, anchor='n', fill="x")

        show_closed_image_checkbutton.pack(expand=True, anchor='n', fill="x")
        show_valley_field_checkbutton.pack(expand=True, anchor='n', fill="x")
        show_all_pos_eyes_checkbutton.pack(expand=True, anchor='n', fill="x")
        show_pos_eyes_checkbutton.pack(expand=True, anchor='n', fill="x")
        after_rotation_checkbutton.pack(expand=True, anchor='n', fill="x")
        show_final_checkbutton.pack(expand=True, anchor='n', fill="x")
        show_info_checkbutton.pack(expand=True, anchor='n', fill="x")

        style_trans_options_frame.pack(expand=True, fill="both", side=tkinter.LEFT)
        style_trans_options_frame.pack_propagate(0)
        title_style_label.pack(expand=True, anchor='n', fill="x")

        iterations_frame.pack(expand=True, fill="both")
        num_of_iter_label.pack(expand=True, anchor='n', padx=5, side=tkinter.LEFT)
        max_iter_entry.pack(expand=True, anchor='n', padx=5, side=tkinter.LEFT)

        interval_frame.pack(expand=True, fill="both")
        interval_frame.pack_propagate(0)
        show_process_of_tran_checkbutton.pack(expand=True, anchor='n', fill="x")
        progress_update_label.pack(expand=True, anchor='n', padx=5, side=tkinter.LEFT)
        self.progress_update_inter_entry.pack(expand=True, anchor='n', padx=5, side=tkinter.LEFT)

        save_button.pack(expand=True, side=tkinter.BOTTOM, anchor='s')
        return

    def test_and_update_remote_info(self):
        """
        TODO: Fill that
        """
        host = self.h.get()
        user_name = self.un.get()
        port = self.po.get()
        user_password = self.up.get()
        private_key_path = self.private_key_path.get()
        private_key_password = self.private_key_pass.get()
        sftp = None
        try:
            sftp = utils.connect_to_sftp_server(host=host, port=port, username=user_name, password=user_password,
                                                private_key=private_key_path, private_key_pass=private_key_password,
                                                test=True)
            directories = sftp.listdir()
            print("directory list in public folder of server:" + str(directories))
            sftp.close()
        except (ValueError, AttributeError) as eroor:
            if type(sftp) is str:
                utils.popup_msg("Connection Test FAILED!",
                                "You were unable to connect to the server.\n" + sftp)
            else:
                utils.popup_msg("Connection Test FAILED!",
                                "You were unable to connect to the server.\n see log for more info.")
            print("FAILED:" + str(eroor))
            return

        utils.REMOTE_USER = user_name
        utils.HOST = host
        utils.REMOTE_PORT = port
        utils.REMOTE_USER_PASSWORD = user_password
        utils.REMOTE_PRIVATE_KEY_PATH = private_key_path
        utils.REMOTE_PRIVATE_KEY_PASSWORD = private_key_password
        utils.popup_msg("Connection Test PASS", "You were able to connect to the server successfully\n HURRAY!")
        return

    def save_and_close(self):
        if self.show_process.get():
            utils.SHOW_PROGRESS = True
            if utils.represents_int(self.process_update_string_var.get()):
                utils.DEFAULT_PROGRESS_UPDATE_INTERVAL = int(self.process_update_string_var.get())
            else:
                utils.popup_msg("WARNING", "Value in field \"Progress Update Interval::\" (" +
                                self.process_update_string_var.get() + ")\nIs not a whole number (not an integer) "
                                                                       "\nPlease correct error and try again.")
                return

        else:
            utils.SHOW_PROGRESS = False
        self.host = self.h.get()
        utils.HOST = self.host
        self.user_name = self.un.get()
        utils.REMOTE_USER = self.user_name
        self.port = self.po.get()
        utils.REMOTE_PORT = self.port
        self.user_password = self.up.get()
        utils.REMOTE_USER_PASSWORD = self.user_password
        self.private_key_path = self.private_key_path.get()
        utils.REMOTE_PRIVATE_KEY_PATH = self.private_key_path
        self.private_key_password = self.private_key_pass.get()
        utils.REMOTE_PRIVATE_KEY_PASSWORD = self.private_key_password
        utils.DEBUG_SHOW_CLOSED_IMAGE_MODE = self.int_vars_debug_opts[0].get()
        utils.DEBUG_SHOW_VALLY_FIELD_MODE = self.int_vars_debug_opts[1].get()
        utils.DEBUG_SHOW_ALL_POSSIBLE_EYES_MODE = self.int_vars_debug_opts[2].get()
        utils.DEBUG_SHOW_BEST_POSSIBLE_EYES_MODE = self.int_vars_debug_opts[3].get()
        utils.DEBUG_SHOW_AFTER_ROTATE_MODE = self.int_vars_debug_opts[4].get()
        utils.DEBUG_SHOW_FINAL_RESULTS = self.int_vars_debug_opts[5].get()
        utils.DEBUG_INFO_MODE = self.int_vars_debug_opts[6].get()
        if utils.represents_int(self.max_inter.get()):
            utils.DEFAULT_TRANSFER_ITERATIONS = int(self.max_inter.get())
        else:
            utils.popup_msg("WARNING", "Value in field \"Number of Iterations:\" (" + self.max_inter.get() +
                            ") \nIs not a whole number (not an integer)\nPlease correct error and try again.")
            return
        self.settings.destroy()
        return

    def toggle_show_progress(self):
        """
        TODO: Fill that
        """
        if self.show_process.get():
            self.progress_update_inter_entry.config(state='normal')
        else:
            self.progress_update_inter_entry.config(state='disable')
        return
