import argparse
import os
import sys


# Adding project to system path to be able to import other files from project
path = '/'.join(os.path.dirname(os.path.realpath(__file__)).split('/')[0:-2])
if path not in sys.path:
    print("adding " + path + " to sys.path")
    sys.path.insert(0, path)

import src.utils as utils

parser = argparse.ArgumentParser(description='run style transformation on images.')
parser.add_argument('-content_path', metavar='</path/to/content/image>', type=str, nargs=1, required=True,
                    help='This should contain the full path to the content image.')
parser.add_argument('-style_path', metavar='</path/to/style/image>', type=str, nargs=1, required=True,
                    help='This should contain the full path to the style image.')
parser.add_argument('-output_path', metavar='</path/to/result/directory/>', type=str, nargs=1, required=True,
                    help='This should contain the full path to the output folder location.'
                         'That location will be used to save the result image. '
                         'The result image will be names \"result.jpg\" at the result dir location.')
parser.add_argument('-alg', metavar='algorithm', nargs=1, default="naas", choices=['naas', 'style_only', 'fast_trans'],
                    help='The algorithm to run for style transformation.'
                         '2 options exists. \"naas\" or \"style_only\". default value is \"naas\"')
parser.add_argument('-fast_trans_style', metavar='algorithm', nargs=1,
                    help='The fast transformation algorithm to run for style transformation.'
                         'This should include the name of the pre trained model. '
                         'It should hold an existing pre trained model name.')
parser.add_argument('-show_progress', type=int,  nargs='?', default=0,
                    help='save progress files to progress folder in output dir. '
                         'value of this should be the update interval.'
                         'Use 0 (zero) to disable progress update. default is 0 (zero)')
parser.add_argument('-max_iter', type=int, nargs='?', default=utils.DEFAULT_TRANSFER_ITERATIONS,
                    help='save progress files to progress folder in output dir. '
                         'value of this should be the update interval.'
                         'Use 0 (zero) to disable progress update. default is 0 (zero)')

args = parser.parse_args()
print(args)
content_path = args.output_path[0] + 'resized_content_thumb.jpg'
style_path = args.output_path[0] + 'resized_style_thumb.jpg'
utils.DEFAULT_TRANSFER_ITERATIONS = args.max_iter
if args.alg[0] == "naas":
    import src.style_transform.neural_algorithm_Art_sty as naas
    naas.naas_algorithm(style_path, content_path, args.output_path[0], show_iter=args.show_progress,
                        max_iter=args.max_iter)
elif args.alg[0] == "style_only":
    import src.style_transform.style_transformer as st
    st.run_style_transfer(content_path, style_path, save_dir=args.output_path[0], show_iter=args.show_progress,
                          num_iterations=args.max_iter)
elif args.alg[0] == "fast_trans":
    cmd = 'pipenv run python evaluate.py '
    cmd += '--checkpoint /home/tibi/Public/' + args.fast_trans_style[0] + '.ckpt '
    cmd += '--in-path ' + style_path
    cmd += ' --out-path ' + args.output_path[0] + 'result.jpg '
    cmd += '--allow-different-dimensions'
    print("running command on server:" + cmd)
    stdout = utils.subprocess_cmd("cd /home/tibi/fast-style-transfer-master ;" + cmd)
    for line in stdout.decode("utf-8"):
        print(line, end="")
    print('finished.')


exit(0)
