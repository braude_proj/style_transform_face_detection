import time

import IPython.display
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import tensorflow.contrib.eager as tfe
from tensorflow.python.keras import models
from tensorflow.python.keras.preprocessing import image as kp_image

# Content layer where will pull our feature maps
import src.utils as utils

content_layers = ['block5_conv2']

# Style layer we are interested in
style_layers = ['block1_conv1',
                'block2_conv1',
                'block3_conv1',
                'block4_conv1',
                'block5_conv1'
                ]

num_content_layers = len(content_layers)
num_style_layers = len(style_layers)


def load_img(path_to_img):
    """
    Load the style image, resize it, convert it to array and expand its shape
    :param path_to_img: path to the style image
    :return: array of the resized and expanded style image
    """
    max_dim = 256
    img = utils.Image.open(path_to_img)
    long = max(img.size)
    scale = max_dim / long
    img = img.resize((round(img.size[0] * scale), round(img.size[1] * scale)), utils.Image.ANTIALIAS)

    img = kp_image.img_to_array(img)

    # We need to broadcast the image array such that it has a batch dimension
    img = np.expand_dims(img, axis=0)
    return img


def load_and_process_img(path_to_img):
    img = load_img(path_to_img)
    img = tf.keras.applications.vgg19.preprocess_input(img)
    return img


def deprocess_img(processed_img):
    x = processed_img.copy()
    if len(x.shape) == 4:
        x = utils.np.squeeze(x, 0)
    assert len(x.shape) == 3, ("Input to deprocess image must be an image of "
                               "dimension [1, height, width, channel] or [height, width, channel]")
    if len(x.shape) != 3:
        raise ValueError("Invalid input to deprocessing image")

    # perform the inverse of the postprocessing step
    x[:, :, 0] += 103.939
    x[:, :, 1] += 116.779
    x[:, :, 2] += 123.68
    x = x[:, :, ::-1]

    x = utils.np.clip(x, 0, 255).astype('uint8')
    return x


def get_model():
    """
    Creates keras applications model with access to intermediate layers.
    This function will load the VGG19 model and access the intermediate layers.
    These layers will then be used to create a new model that will take input image
    and return the outputs from these intermediate layers from the VGG model.

    :return: keras model that takes image inputs and outputs the style and content intermediate layers.
    """

    # Load our model. We load pre trained VGG, trained on imagenet data
    vgg = tf.keras.applications.vgg19.VGG19(include_top=False, weights='imagenet')
    vgg.trainable = False

    # Get output layers corresponding to style and content layers
    style_outputs = [vgg.get_layer(name).output for name in style_layers]
    content_outputs = [vgg.get_layer(name).output for name in content_layers]

    # Build model
    model_outputs = style_outputs + content_outputs
    return models.Model(vgg.input, model_outputs)


def get_content_loss(base_content, target):
    """
    Calculate the content loss
    :param base_content: TODO: Fill that
    :param target: TODO: Fill that
    :return: Content loss
    """
    return tf.reduce_mean(tf.square(base_content - target))


def gram_matrix(input_tensor):
    """
    TODO: Fill that
    :param input_tensor: TODO: Fill that
    :return: TODO: Fill that
    """

    # We make the image channels first
    channels = int(input_tensor.shape[-1])
    a = tf.reshape(input_tensor, [-1, channels])
    n = tf.shape(a)[0]
    gram = tf.matmul(a, a, transpose_a=True)
    return gram / tf.cast(n, tf.float32)


def get_style_loss(base_style, gram_target):
    """
    Expects two images of dimension h, w, c
    :param base_style: TODO: Fill that
    :param gram_target: TODO: Fill that
    :return: TODO: Fill that
    """

    # height, width, num filters of each layer
    # We scale the loss at a given layer by the size of the feature map and the number of filters
    height, width, channels = base_style.get_shape().as_list()  # TODO: why that is needed if the values are not in use?
    gram_style = gram_matrix(base_style)

    return tf.reduce_mean(tf.square(gram_style - gram_target))  # / (4. * (channels ** 2) * (width * height) ** 2)


def get_feature_representations(model, content_path, style_path):
    """
    Helper function to compute our content and style feature representations.
    This function will simply load and preprocess both the content and style
    images from their path. Then it will feed them through the network to obtain
    the outputs of the intermediate layers
    :param model:  The model that we are using
    :param content_path: The path to the content image
    :param style_path: The path to the style image
    :return: returns the style features and the content features
    """

    # Load the style and content images
    content_image = load_and_process_img(content_path)
    style_image = load_and_process_img(style_path)

    # Batch compute content and style features
    style_outputs = model(style_image)
    content_outputs = model(content_image)

    # Get the style and content feature representations from our model
    style_features = [style_layer[0] for style_layer in style_outputs[:num_style_layers]]
    content_features = [content_layer[0] for content_layer in content_outputs[num_style_layers:]]
    return style_features, content_features


def compute_loss(model, loss_weights, init_image, gram_style_features, content_features):
    """
    Computes the total loss
    :param model: The model that will give us access to the intermediate layers
    :param loss_weights: The weights of each contribution of each loss function
    (style weight, content weight, and total variation weight)
    :param init_image: Our initial base image. This image is what we are updating with
    our optimization process. We apply the gradients wrt the loss we are calculating to this image
    :param gram_style_features: Precomputed gram matrices corresponding to the defined style layers of interest
    :param content_features: Precomputed outputs from defined content layers of interest
    :return:  The total loss, style loss, content loss, and total variational loss
    """
    style_weight, content_weight = loss_weights

    # Feed our init image through our model. This will give us the content and
    # style representations at our desired layers. Since we're using eager
    # our model is callable just like any other function!
    model_outputs = model(init_image)

    style_output_features = model_outputs[:num_style_layers]
    content_output_features = model_outputs[num_style_layers:]

    style_score = 0
    content_score = 0

    # Accumulate style losses from all layers
    # Equally weight each contribution of each loss layer
    weight_per_style_layer = 1.0 / float(num_style_layers)
    for target_style, comb_style in zip(gram_style_features, style_output_features):
        style_score += weight_per_style_layer * get_style_loss(comb_style[0], target_style)

    # Accumulate content losses from all layers
    weight_per_content_layer = 1.0 / float(num_content_layers)
    for target_content, comb_content in zip(content_features, content_output_features):
        content_score += weight_per_content_layer * get_content_loss(comb_content[0], target_content)

    style_score *= style_weight
    content_score *= content_weight

    # Get total loss
    loss = style_score + content_score
    return loss, style_score, content_score


def compute_grads(cfg):
    """
    TODO: Fill that
    :param cfg: TODO: Fill that
    :return: TODO: Fill that
    """
    with tf.GradientTape() as tape:
        all_loss = compute_loss(**cfg)
    # Compute gradients wrt input image
    total_loss = all_loss[0]
    return tape.gradient(total_loss, cfg['init_image']), all_loss


def run_style_transfer(content_path, style_path, num_iterations=utils.DEFAULT_TRANSFER_ITERATIONS, content_weight=1e3,
                       style_weight=1e-2, save_dir=utils.RESULTS_DIR, show_iter=utils.DEFAULT_PROGRESS_UPDATE_INTERVAL):
    """
    TODO: Fill that
    :param content_path: The path of the content image
    :param style_path: The path of the style image
    :param num_iterations: number of iterations that the style transfer will run
    (Determines the quality of the style transfer)
    :param content_weight: TODO: Fill that
    :param style_weight: TODO: Fill that
    :param save_dir: The directory where the result image will be saved
    :param show_iter: Number of iterations that will show the user the progress of the style transfer
    :return: TODO: Fill that
    """
    tf.enable_eager_execution()
    print("Eager execution: {}".format(tf.executing_eagerly()))
    # We don't need to (or want to) train any layers of our model, so we set their
    # trainable to false.
    model = get_model()
    for layer in model.layers:
        layer.trainable = False

    # Get the style and content feature representations (from our specified intermediate layers)
    style_features, content_features = get_feature_representations(model, content_path, style_path)
    gram_style_features = [gram_matrix(style_feature) for style_feature in style_features]

    # Set initial image
    init_image = load_and_process_img(content_path)
    init_image = tfe.Variable(init_image, dtype=tf.float32)
    # Create our optimizer
    opt = tf.train.AdamOptimizer(learning_rate=5, beta1=0.99, epsilon=1e-1)

    # Store our best result
    best_loss, best_img = float('inf'), None

    # Create a nice config
    loss_weights = (style_weight, content_weight)
    cfg = {
        'model': model,
        'loss_weights': loss_weights,
        'init_image': init_image,
        'gram_style_features': gram_style_features,
        'content_features': content_features
    }

    # For displaying
    num_rows = 2
    num_cols = 5
    display_interval = num_iterations / (num_rows * num_cols)
    global_start = time.time()

    norm_means = utils.np.array([103.939, 116.779, 123.68])
    min_values = -norm_means
    max_values = 255 - norm_means

    images = []
    for i in range(num_iterations+1):
        if show_iter != 0 and (i % show_iter) == 0:
            utils.Image.fromarray(deprocess_img(init_image.numpy())).save("{}{}".format(save_dir + "progress/", i),
                                                                          "JPEG")
        grads, all_loss = compute_grads(cfg)
        loss, style_score, content_score = all_loss
        opt.apply_gradients([(grads, init_image)])
        clipped = tf.clip_by_value(init_image, min_values, max_values)
        init_image.assign(clipped)

        if loss < best_loss:
            # Update best loss and best image from total loss.
            best_loss = loss
            best_img = deprocess_img(init_image.numpy())

        if i % display_interval == 0:
            start_time = time.time()

            # Use the numpy() method to get the concrete numpy array
            plot_img = init_image.numpy()
            plot_img = deprocess_img(plot_img)
            images.append(plot_img)
            IPython.display.clear_output(wait=True)
            IPython.display.display_png(utils.Image.fromarray(plot_img))
            print('Iteration: {}'.format(i))
            print('Total loss: {:.4e}, '
                  'style loss: {:.4e}, '
                  'content loss: {:.4e}, '
                  'time: {:.4f}s'.format(loss, style_score, content_score, time.time() - start_time))
            # TODO: add some mechanism to tell client the current progress
    print('Total time: {:.4f}s'.format(time.time() - global_start))
    IPython.display.clear_output(wait=True)
    plt.figure(figsize=(14, 4))
    for i, img in enumerate(images):
        plt.subplot(num_rows, num_cols, i + 1)
        plt.imshow(img)
        plt.xticks([])
        plt.yticks([])
    pil_image = utils.Image.fromarray(best_img)
    pil_image.save(save_dir + "result.jpg", "JPEG")
    return best_img, best_loss
