import math
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

from src import utils
from src.face_detection.shared_variables_face_detection import EDGE_LENGTH_X, EDGE_LENGTH_Y

MAX_HEAD_ANGLE = 45


class FaceRegion:
    EYE_DISTANCE_AND_FACE_HEIGHT_RATIO = 1.8  # Not changeable
    EYE_DISTANCE_AND_FACE_WIDTH_RATIO = 1.85  # Not changeable
    EYE_DISTANCE_AND_EYE_HEIGHT_RATIO = 1 / 4  # Not changeable
    EYE_DISTANCE_AND_EYE_WIDTH_RATIO = 0.2  # Not changeable

    RESCALED_FACE_REGION_EDGE_LENGTH_X = 28  # Not changeable
    RESCALED_FACE_REGION_EDGE_LENGTH_Y = 31  # Not changeable

    MAX_FACES_IN_LINE = 11  # Changeable
    MIN_DISTANCE_BETWEEN_EYES = EDGE_LENGTH_X / MAX_FACES_IN_LINE  # Not changeable

    MIN_HEAD_ANGLE = 10  # Not changeable
    counter = 0

    def __init__(self, eye1_score, eye2_score, old_eye1, old_eye2, eye1, eye2, angle, start_point, left_lower,
                 end_point, right_upper, height, width, image_arr, image_name):
        self.eyes_score = (eye1_score + eye2_score) / 2

        self.x0 = eye1[1]
        self.x0_before_rescale = self.x0
        self.y0 = eye1[0]
        self.y0_before_rescale = self.y0

        self.x1 = eye2[1]
        self.x1_before_rescale = self.x1
        self.y1 = eye2[0]
        self.y1_before_rescale = self.y1

        self.old_eye1 = old_eye1
        self.old_eye2 = old_eye2

        self.angle = angle

        self.start_point = start_point
        self.left_lower = left_lower
        self.end_point = end_point
        self.right_upper = right_upper

        self.height = height
        self.width = width

        # At the initialization the self.image_arr will represent all the content image, after the rescale,
        # self.image_arr will represent the face image
        self.image_arr = image_arr

        self.index = FaceRegion.counter

        self.image_name = image_name

        if utils.DEBUG_SHOW_AFTER_ROTATE_MODE:
            # Present the face with mark of the possible eyes after rotating the face
            # so the possible face will stand strait
            temp_image = self.image_arr.copy()
            temp_image.setflags(write=1)
            buffer = int(max(temp_image.shape)/100)
            temp_image[self.y0-buffer:self.y0+buffer+1, self.x0-buffer:self.x0+buffer+1] = 255
            temp_image[self.y1-buffer:self.y1+buffer+1, self.x1-buffer:self.x1+buffer+1] = 255
            temp_image[self.y0][self.x0] = 0
            temp_image[self.y1][self.x1] = 0
            self.image_arr = temp_image
            title = self.image_name + "," + str(self.index)
            if utils.show_image(self.image_arr, title, is_arr=True, regression_candidate=True):
                utils.DEBUG_SKIP_FACE_SWITCH = True

        self.rescale()
        self.symmetry = utils.arr_symmetry(self.image_arr)

    @staticmethod
    def create_face_region_if_possible(eye1, eye2, image_arr):
        """
        Check if couple of eye can create a face on the image
        :param eye1: score of the first eye and it's coordinates
        :param eye2: score of the second eye and it's coordinates
        :param image_arr: arr that represent the image where the possible eyes were found
        :return: False if the given eyes cant create possible face, else return information about the possible face
        """
        # In eye[0] the score is saved
        y0 = eye1[1][0]
        x0 = eye1[1][1]

        y1 = eye2[1][0]
        x1 = eye2[1][1]

        distance_eyes = math.sqrt(((x1 - x0) ** 2) + ((y1 - y0) ** 2))
        if distance_eyes <= FaceRegion.MIN_DISTANCE_BETWEEN_EYES:
            return False

        if y1 != y0:  # Check if there is angle between the eyes
            head_angle = math.degrees(math.atan(-((x1 - x0) / (y1 - y0))))
            if head_angle <= 0:
                head_angle = 90 - abs(head_angle)
            else:
                head_angle -= 90

        else:  # y1 == y0
            head_angle = 0

        if abs(head_angle) > MAX_HEAD_ANGLE:
            return False

        (new_x0, new_y0) = utils.new_position_after_rotate(x0, y0, head_angle, EDGE_LENGTH_X, EDGE_LENGTH_Y)
        (new_x1, new_y1) = utils.new_position_after_rotate(x1, y1, head_angle, EDGE_LENGTH_X, EDGE_LENGTH_Y)

        image_arr = Image.fromarray(image_arr)
        image_arr = image_arr.rotate(head_angle, resample=Image.BICUBIC)
        image_arr = np.asarray(image_arr)

        face_height = int(FaceRegion.EYE_DISTANCE_AND_FACE_HEIGHT_RATIO * distance_eyes)
        face_width = int(FaceRegion.EYE_DISTANCE_AND_FACE_WIDTH_RATIO * distance_eyes)
        eye_height = int(FaceRegion.EYE_DISTANCE_AND_EYE_HEIGHT_RATIO * face_height)
        eye_width = int(FaceRegion.EYE_DISTANCE_AND_EYE_WIDTH_RATIO * face_height)

        # Set the start point(upper left point) and end point(lower right point) of the possible face
        if new_x0 < new_x1:
            start_x = new_x0 - eye_width
        else:
            start_x = new_x1 - eye_width

        start_y = new_y0 - eye_height
        start_point = (start_x, start_y)  # Upper left conner of the face region
        left_lower = (start_x ,start_y + face_height)
        right_upper = (start_x + face_width ,start_y)
        end_point = (start_x + face_width, start_y + face_height)  # Left button point of the detected face

        # Check if the defined region is possible in the image (possible if all the region is on the image)
        if start_point[0] < 0 or start_point[1] < 0:
            return False
        if end_point[0] >= image_arr.shape[1] or end_point[1] >= image_arr.shape[0]:
            return False

        return eye1[0], eye2[0], (x0, y0), (x1, y1), (new_y0, new_x0), (new_y1, new_x1), head_angle, start_point, \
               left_lower, end_point, right_upper, face_height, face_width, image_arr

    def rescale(self):
        """
        Crop the image to the place where the possible face is
        """
        #  TODO: Change the writing style(without loops) + self.height and self.width are not really needed
        #   (can work with start point and point)
        data_image = np.zeros((self.height, self.width))
        for i in range(0, self.height):
            for j in range(0, self.width):
                data_image[i][j] = self.image_arr[self.start_point[1] + i][self.start_point[0] + j]

        new_ratio_x = self.RESCALED_FACE_REGION_EDGE_LENGTH_X / EDGE_LENGTH_X
        new_ratio_y = self.RESCALED_FACE_REGION_EDGE_LENGTH_Y / EDGE_LENGTH_Y

        # Change the eyes coordinates related to the re scaled image
        self.x0 = int(new_ratio_x * self.x0)
        self.y0 = int(new_ratio_y * self.y0)

        self.x1 = int(new_ratio_x * self.x1)
        self.y1 = int(new_ratio_y * self.y1)

        # TODO: Fix lighting fix
        # data_image = utils.fix_lighting(data_image)

        # Change the image size to RESCALED_FACE_REGION_EDGE_LENGTH_X x RESCALED_FACE_REGION_EDGE_LENGTH_Y
        self.image_arr = np.asarray(Image.fromarray(data_image).resize(
            (FaceRegion.RESCALED_FACE_REGION_EDGE_LENGTH_X, FaceRegion.RESCALED_FACE_REGION_EDGE_LENGTH_Y),
            resample=Image.BILINEAR))

        if utils.DEBUG_SHOW_RESCALED_IMAGE:
            utils.show_image(self.image_arr, "Rescaled", is_arr=True)

    def check_existence_of_facial_features(self):
        """
        Check if the possible face contain facial features(eyes, eyebrows, nose and mouth)
        :return: False if the possible face don't contain one of the facial features, else True
        """
        if not utils.DEBUG_INDEX_FACE or self.index in utils.DEBUG_INDEX:
            if not self.check_eyes_and_eyebrows_y():
                return False
            if not self.check_nose_y():
                return False
            if not self.check_mouth_y():
                return False
            if not self.check_eyes_and_eyebrows_and_mouth_x():
                return False
            if not self.check_nose_x():
                return False
        else:
            return False

        return True

    def check_eyes_and_eyebrows_y(self):
        """
        Check if the possible face contain eyes and eyebrows by projection in y direction
        :return: False if the possible face don't contain eyes or eyebrows according to the projection in the
         y direction else True
        """

        # From the given image (rescaled) crop the the upper part of the image
        # (where the eyes and eyebrows should be found)
        h = int(FaceRegion.RESCALED_FACE_REGION_EDGE_LENGTH_Y / 3)
        cut_offset = int(0.12 * h)
        w = FaceRegion.RESCALED_FACE_REGION_EDGE_LENGTH_X - cut_offset
        upper_region = self.image_arr[0:h, cut_offset:w-cut_offset]

        y1 = utils.y_projection(upper_region)
        minimum_count, end_min, start_min, peaks, min_properties = utils.local_minimums(y1, distance=1)
        if utils.DEBUG_SHOW_PROJECTION_MODE or (utils.DEBUG_REGRESSION_MODE and self.index in utils.DEBUG_INDEX):
            utils.show_plot_function(y1, is_x_projection=False, title="Eyes (expect 2 minimums)", peaks=peaks)
            utils.show_image(upper_region, "Eyes", is_arr=True)
            plt.close()

        # Check if the projection in that part have two minimums, one for the eyebrows and the other for the eyes
        minimum_count, end_min, start_min, peaks, min_properties = utils.local_minimums(y1)
        if minimum_count == 2 or minimum_count + start_min == 2:
            return True

        minimum_count, end_min, start_min, peaks, min_properties = utils.local_minimums(y1, distance=1, prominence=30)
        if utils.DEBUG_SHOW_PROJECTION_MODE or (utils.DEBUG_REGRESSION_MODE and self.index in utils.DEBUG_INDEX):
            utils.show_plot_function(y1, is_x_projection=False, title="Eyes (expect 1 minimums)", peaks=peaks)
            utils.show_image(upper_region, "Eyes", is_arr=True)
            plt.close()

        # Sometime the minimum of the eyes and the eyebrows are combined (especially in angry faces)
        minimum_count, end_min, start_min, peaks, min_properties = utils.local_minimums(y1)
        if minimum_count == 1:
            return True

        return False

    def check_nose_y(self):
        """
        Check if the possible face contain nose by projection in y direction
        :return: False if the possible face don't contain nose according to the projection in the y direction
        else True
        """

        # From the given image (rescaled) crop the the middle part of the image
        # (where the nose should be found)
        h = int(FaceRegion.RESCALED_FACE_REGION_EDGE_LENGTH_Y / 3)
        # TODO: Not clear why doesn't x_eye1 = self.x0 and x_eye2 = self.x1... figure that out
        x_eye1 = int(self.EYE_DISTANCE_AND_EYE_WIDTH_RATIO * h)
        x_eye2 = FaceRegion.RESCALED_FACE_REGION_EDGE_LENGTH_X - int(self.EYE_DISTANCE_AND_EYE_WIDTH_RATIO * h)
        mid_region = self.image_arr[h:2 * h, x_eye1:x_eye2]

        y1 = utils.y_projection(mid_region)
        minimum_count, end_min, start_min, peaks, min_properties = utils.local_minimums(y1)
        if utils.DEBUG_SHOW_PROJECTION_MODE or (utils.DEBUG_REGRESSION_MODE and self.index in utils.DEBUG_INDEX):
            utils.show_plot_function(y1, is_x_projection=False, title=" Nose (expect 1 minimum)", peaks=peaks)
            utils.show_image(mid_region, "Nose", EDGE_LENGTH_X, is_arr=True)
            plt.close()

        # Check if the projection in that part have one minimum, for the nostrils. We will refer to
        # start minimum as a minimum also because the nostrils may be found in the lowest part of the image
        if minimum_count == 1 or start_min == 1:
            return True

        return False

    def check_mouth_y(self):
        """
        Check if the possible face contain mouth by projection in y direction
        :return: False if the possible face don't contain mouth according to the projection in the y direction
        else True
        """

        # From the given image (rescaled) crop the the lower part of the image
        # (where the mouth should be found)
        h = int(FaceRegion.RESCALED_FACE_REGION_EDGE_LENGTH_Y / 3)
        cut_offset = int(h / 3 + 0.12 * h)
        lower_region = self.image_arr[2 * h:3 * h, cut_offset:FaceRegion.RESCALED_FACE_REGION_EDGE_LENGTH_X-cut_offset]

        y1 = utils.y_projection(lower_region)
        minimum_count, end_min, start_min, peaks, min_properties = utils.local_minimums(y1, prominence=6)
        if utils.DEBUG_SHOW_PROJECTION_MODE or (utils.DEBUG_REGRESSION_MODE and self.index in utils.DEBUG_INDEX):
            utils.show_plot_function(y1, is_x_projection=False,
                                     title=" Mouth (expect 1 minimum or 2 but one significant)", peaks=peaks)
            utils.show_image(lower_region, "Mouth", is_arr=True)
            plt.close()

        # Check if the projection in that part have one minimums, for the mouth. We will refer to
        # start minimum as a minimum also because the mouth may be found in the lowest part of the image
        # If one minimums was not found, we will check if there are two minimums, one for the mouth and the other for
        # wrinkle or beard under the mouth
        if minimum_count == 1 or start_min == 1:
            return True
        if minimum_count == 2 and min_properties['prominences'][0] > min_properties['prominences'][1] * 2:
            return True

        return False

    def check_eyes_and_eyebrows_and_mouth_x(self):
        """
        Check if the possible face contain eyes, eyebrows and mouth by projection in x direction
        :return: False if the possible face don't contain eyes, eyebrows and mouth according to the projection in the x
        direction else True
        """

        # Note: a rescale was not needed because in that projection we want to project on the whole face
        x1 = utils.x_projection(self.image_arr)
        minimum_count, end_min, start_min, peaks, min_properties = utils.local_minimums(x1, distance=5, prominence=1.5)
        if utils.DEBUG_SHOW_PROJECTION_MODE or (utils.DEBUG_REGRESSION_MODE and self.index in utils.DEBUG_INDEX):
            utils.show_plot_function(x1, is_x_projection=True, title=" Eyes eyebrows and mouth (expect 2 minimums)",
                                     peaks=peaks)
            utils.show_image(self.image_arr, "Eyes eyebrows and mouth", is_arr=True)
            plt.close()

        # Check if the projection in that part have two minimums,
        # one for the left eyebrow, eye and left part of the start of the mouth
        # and the other for the right eyebrow, eye and left part of the start of the mouth
        if minimum_count == 2:
            return True

        return False

    def check_nose_x(self):
        """
        Check if the possible face contain nose by projection in x direction
        :return: False if the possible face don't contain nose according to the projection in the x direction else True
        """

        # From the given image (rescaled) crop the the middle part of the image
        # (where the nose should be found)
        h = int(FaceRegion.RESCALED_FACE_REGION_EDGE_LENGTH_Y / 3)
        # TODO: Not clear why doesn't x_eye1 = self.x0 and x_eye2 = self.x1... figure that out
        x_eye1 = int(self.EYE_DISTANCE_AND_EYE_WIDTH_RATIO * h)
        x_eye2 = FaceRegion.RESCALED_FACE_REGION_EDGE_LENGTH_X - int(self.EYE_DISTANCE_AND_EYE_WIDTH_RATIO * h)
        nose_region = self.image_arr[h:2 * h, x_eye1:x_eye2]

        x1 = utils.x_projection(nose_region)
        minimum_count, end_min, start_min, peaks, min_properties = utils.local_minimums(x1)
        if utils.DEBUG_SHOW_PROJECTION_MODE or (utils.DEBUG_REGRESSION_MODE and self.index in utils.DEBUG_INDEX):
            utils.show_plot_function(x1, is_x_projection=True, title=" Nose (expect 2 minimums)", peaks=peaks)
            utils.show_image(nose_region, "Nose", is_arr=True)
            plt.close()

        # Check if the projection in that part have two minimums, one for each nostril
        if minimum_count == 2:
            return True

        return False
