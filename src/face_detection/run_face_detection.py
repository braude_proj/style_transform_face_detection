import numpy as np

import src.face_detection.face_region as face_region
from src import utils
from src.face_detection.eyes_detector import EyesDetector

SYMMETRY_THRESHOLD = 35  # Changeable


def detect_faces(image_for_detection, image_name):
    """
    Detect faces in given image
    :param image_name: name of image we are working on. used for debugging and general information
    :param image_for_detection: Pil image from which the detection will find faces
    :return: list of rectangles that represent faces that found on the given image
    """

    gray_image = image_for_detection.convert('L')  # Convert RGB image to grey-scale image
    gray_image_arr = np.asarray(gray_image)  # Convert grey-scale image to arr
    if utils.DEBUG_SHOW_GRAY_IMAGE_MODE:
        utils.show_image(gray_image, 'Gray Image')

    # gray_image_arr = utils.adjust_gamma(gray_image_arr, gamma=2)  # Adjust brightness

    # Detect possible eyes in the given arr (represent gray image)
    eyes_detector = EyesDetector(gray_image_arr, image_name=image_name)
    potential_faces = eyes_detector.define_potential_face_regions()  # Define potential faces from the detected eyes

    potential_faces.sort(key=lambda x: x.symmetry, reverse=False)

    symmetric_faces_count = 0
    faces_count = 0
    detected_faces = list()

    for possible_face in potential_faces:
        # Check if the possible face region is symmetric enough אto be face
        if possible_face.symmetry > SYMMETRY_THRESHOLD:
            if utils.DEBUG_REGRESSION_MODE and possible_face.index in utils.DEBUG_INDEX:
                print("ALMOST SKIPPED WANTED FACE INDEX " + str(possible_face.index))
                utils.popup_msg("SKIPPING WANTED FACE!", "you asked to debug face index:" +
                                str(possible_face.index) + " but it has symmetry: " +
                                str(possible_face.symmetry) + ">" + str(SYMMETRY_THRESHOLD))
            else:
                continue
        symmetric_faces_count += 1
        if utils.DEBUG_SHOW_SYMMETRY_FACES:
            gray_image_temp = gray_image_arr
            utils.mark_square_on_image(gray_image_temp, possible_face.start_point, possible_face.end_point,
                                       possible_face.angle,
                                       points=((possible_face.x0_before_rescale, possible_face.y0_before_rescale),
                                               (possible_face.x1_before_rescale, possible_face.y1_before_rescale)),
                                       is_arr=True, title=possible_face.symmetry)

        # Check if the possible face region contain facial features
        if possible_face.check_existence_of_facial_features():
            faces_count += 1

            # eye1 = (possible_face.old_eye1[1], possible_face.old_eye1[0])
            # eye2 = (possible_face.old_eye2[1], possible_face.old_eye2[0])
            # detected_faces.append(((eye1, eye2), possible_face.angle, possible_face))

            start_point_fix = (possible_face.start_point[1], possible_face.start_point[0])
            left_lower = (possible_face.left_lower[1], possible_face.left_lower[0])
            end_point_fix = (possible_face.end_point[1], possible_face.end_point[0])
            right_upper = (possible_face.right_upper[1], possible_face.right_upper[0])
            detected_faces.append(((start_point_fix, left_lower, end_point_fix, right_upper),
                                   possible_face.angle, possible_face))

            if utils.DEBUG_SHOW_FINAL_RESULTS:
                face_info = "Index- " + str(possible_face.index) + \
                            ": Symmetry- " + str(round(possible_face.symmetry, 2)) + \
                            " Eye's score- " + str(round(possible_face.eyes_score, 2))
                print(face_info)

                utils.present_coordinates([(possible_face.old_eye1[1], possible_face.old_eye1[0]),
                                           (possible_face.old_eye2[1], possible_face.old_eye2[0])],
                                          "Result eyes", gray_image_arr)

    if utils.DEBUG_INFO_MODE:
        print("Number of symmetric faces is " + str(symmetric_faces_count))
        print("Number of final faces is " + str(faces_count))
        print("Finished face detection!")
    face_region.FaceRegion.counter = 0
    return detected_faces
