from operator import itemgetter

from src import utils
from src.face_detection.face_region import FaceRegion
from src.face_detection.shared_variables_face_detection import EDGE_LENGTH_X


class EyesDetector:
    VALLY_FIELD_WEIGHT = 0.7  # Changeable
    NEIGHBORS_WEIGHT = 1 - VALLY_FIELD_WEIGHT  # Not changeable

    NEIGHBOR_DEGREE_LVL1 = 2  # Not changeable
    NEIGHBOR_DEGREE_LVL2 = 3  # Not changeable
    SQUARE_SIZE_LVL1 = 3  # Not changeable
    SQUARE_SIZE_LVL2 = 5  # Not changeable

    VALLY_FIELD_THRESHOLD = 50  # Changeable 15 - 50 -  as this value is higher find less eyes
    WHITE_AREA_IMAGE_THRESHOLD = 100  # Changeable 100 - 210 - as this value is lower find less eyes

    def __init__(self, image, image_name=None):
        self.Image = image
        self.image_name = image_name
        image_vally_field = utils.vally_field(self.Image)
        # Get a list of eye candidates based on the their vally field value
        self.potential_eyes = self.possible_eyes_from_vally_field(image_vally_field)
        # Get a list of the good eye candidates based on the score of each eye
        self.potential_eyes = self.possible_eyes_from_eye_candidates_groups_by_scores(image_vally_field)

    def possible_eyes_from_vally_field(self, vally_field):
        """
        :param vally_field: Vally field of an image
        :return: possible eyes from the vally field
        """
        potential_eyes_from_vally_field = list()
        for i in range(len(vally_field)):
            for j in range(len(vally_field[0])):
                if self.Image[i][j] < self.WHITE_AREA_IMAGE_THRESHOLD and \
                        vally_field[i][j] > self.VALLY_FIELD_THRESHOLD:
                    potential_eyes_from_vally_field.append((i, j))

        if utils.DEBUG_INFO_MODE:
            print("The number of all eyes candidates is " + str(len(potential_eyes_from_vally_field)))

        if utils.DEBUG_SHOW_ALL_POSSIBLE_EYES_MODE:
            utils.present_coordinates(potential_eyes_from_vally_field, "All possible eyes", self.Image)

        return potential_eyes_from_vally_field

    def calculate_eye_score(self, eye, vally_field):
        """
        calculate score for a given eye
        :param eye: Coordinates of possible eye in image
        :param vally_field: Vally field of an image
        :return: Score for the given eye in the image based on the vally field and the neighbors of the pixel
        """

        x = eye[0]
        y = eye[1]

        # Check the average value of the neighbor of the given pixel at distance of
        # NEIGHBOR_DEGREE_LVL1 and NEIGHBOR_DEGREE_LVL2 pixels
        average_value_second_degree_neighbor = utils.average_neighbors_values_in_x(self.Image,
                                                                                   x, y, self.NEIGHBOR_DEGREE_LVL1,
                                                                                   EDGE_LENGTH_X)
        average_value_third_degree_neighbor = utils.average_neighbors_values_in_x(self.Image,
                                                                                  x, y, self.NEIGHBOR_DEGREE_LVL2,
                                                                                  EDGE_LENGTH_X)

        # Check the average value of the square around the given pixel that have the size of
        # (SQUARE_SIZE_LVL1 X SQUARE_SIZE_LVL1) and (SQUARE_SIZE_LVL2 X SQUARE_SIZE_LVL2) pixels
        average_value_in_three_square = utils.average_value_around_coordinate(self.Image, x, y,
                                                                              self.SQUARE_SIZE_LVL1)
        average_value_in_five_square = utils.average_value_around_coordinate(self.Image, x, y,
                                                                             self.SQUARE_SIZE_LVL2)

        # Check the average value of the square around the given pixel in the vally field that have the size of
        # (SQUARE_SIZE_LVL1 X SQUARE_SIZE_LVL1) and (SQUARE_SIZE_LVL2 X SQUARE_SIZE_LVL2) pixels
        average_vally_in_three_square = utils.average_value_around_coordinate(vally_field, x, y,
                                                                              self.SQUARE_SIZE_LVL1)
        average_vally_in_five_square = utils.average_value_around_coordinate(vally_field, x, y,
                                                                             self.SQUARE_SIZE_LVL2)

        if average_value_second_degree_neighbor == -1 or average_value_third_degree_neighbor == -1 or \
                average_value_in_three_square == -1 or average_value_in_five_square == -1 or \
                average_vally_in_three_square == -1 or average_vally_in_five_square == -1:
            return -1
        else:
            return ((average_value_second_degree_neighbor - average_value_in_three_square) * self.NEIGHBORS_WEIGHT
                    + self.VALLY_FIELD_WEIGHT * average_vally_in_three_square,
                    (average_value_third_degree_neighbor - average_value_in_five_square) * self.NEIGHBORS_WEIGHT
                    + self.VALLY_FIELD_WEIGHT * average_vally_in_five_square)

    def possible_eyes_from_eye_candidates_groups_by_scores(self, vally_field):
        """
        Calculate list of best from group of eys in image by flood fill and best score of the eyes in each group
        :param vally_field: Vally field of the image
        :return: List of best from group of eys in image
        """
        already_scored_eyes = list()
        new_potential_eyes = list()

        eyes_map = utils.coordinates_list_to_arr(self.potential_eyes, len(vally_field), len(vally_field[0]))
        for eye in self.potential_eyes:
            if eye in already_scored_eyes:
                continue
            eyes_block = utils.flood_fill(eyes_map, eye[0], eye[1], list())
            already_scored_eyes = already_scored_eyes + eyes_block

            # Get score for each possible eye
            eyes_scores_list = list()
            eyes_in_block_count = 0
            for eye_in_block in eyes_block:
                eye_scores = self.calculate_eye_score(eye_in_block, vally_field)
                if eye_scores == -1:
                    continue

                eyes_in_block_count += 1
                eyes_scores_list.append((max(eye_scores[0], eye_scores[1]), eye_in_block))

            if eyes_in_block_count:
                new_potential_eyes.append(max(eyes_scores_list, key=itemgetter(0)))

        if utils.DEBUG_INFO_MODE:
            print("The number of best eye candidates is " + str(len(new_potential_eyes)))

        if utils.DEBUG_SHOW_BEST_POSSIBLE_EYES_MODE:
            utils.present_coordinates([eye[1] for eye in new_potential_eyes], "Best eye candidates", self.Image)

        return new_potential_eyes

    def define_potential_face_regions(self):
        """
        Tey to define potential face regions according to each couple of eyes
        :return: list of possible potential faces
        """
        potential_faces = list()

        for i in range(len(self.potential_eyes)):
            if utils.DEBUG_SKIP_FACE_SWITCH:
                utils.DEBUG_SKIP_FACE_SWITCH = False
                break
            eye1 = self.potential_eyes[i]
            for j in range(i + 1, len(self.potential_eyes)):
                if utils.DEBUG_SKIP_FACE_SWITCH:
                    break
                eye2 = self.potential_eyes[j]
                possible_face = FaceRegion.create_face_region_if_possible(eye1, eye2, self.Image)
                FaceRegion.counter += 1
                if possible_face:  # The eye in the given image represent possible face
                    potential_faces.append(FaceRegion(*possible_face, self.image_name))

        if utils.DEBUG_INFO_MODE:
            print("The number of possible faces is " + str(len(potential_faces)))
        return potential_faces
