# All global variables and general utilities functions used in the code that doesn't belong to any specific class / tool

import os
import subprocess
import sys
from stat import S_ISDIR, S_ISREG
from tkinter import Tk, ttk

import cv2
import math
import matplotlib.pyplot as plt
import numpy as np
import paramiko
import pysftp
from PIL import Image, ImageTk, ImageDraw
from scipy import ndimage
from scipy.signal import find_peaks
from shapely.geometry import Polygon

# Setting project directory to support both linux and windows


SRC_DIR = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR = '/'.join(SRC_DIR.split('/')[0:-1])
if PROJECT_DIR is '':
    PROJECT_DIR = '/'.join(SRC_DIR.split('\\')[0:-1])
PATH_TO_STATE_DICT = SRC_DIR + r"/style_transform/vgg_conv_weights.pth"
RESULTS_DIR = PROJECT_DIR + r"/results/"
IMAGES_DIR = PROJECT_DIR + r"/images/"
FACES_DIR = IMAGES_DIR + r"faces_a/"
REGRESSION_DIR = IMAGES_DIR + r"regression/"
TRAINING_DIR = REGRESSION_DIR + r"training/"
EYES_DIR = REGRESSION_DIR + r"eyes/"
STYLES_DIR = IMAGES_DIR + r"styles/"
CONTENTS_DIR = IMAGES_DIR + r"contents/"
GUI_IMAGES_DIR = IMAGES_DIR + r"gui/"
SETTING_ICON_PATH = GUI_IMAGES_DIR + "settings.png"
HELP_ICON_PATH = GUI_IMAGES_DIR + "help.png"
DEFAULT_RESULT_PATH = GUI_IMAGES_DIR + "default-result.png"

MAIN_WINDOW = None

IMAGE_EDGE_LENGTH_DISPLAY = 400  # Changeable

IMAGE_MIN_SIZE = 256
IMAGE_MAX_WIDTH = 512

CLOSING_OPERATOR_SIZE = 5  # Changeable

HOST = "delltransfer.ddns.net"
REMOTE_USER = "tibi"
REMOTE_PORT = "22"
REMOTE_USER_PASSWORD = ""
REMOTE_PRIVATE_KEY_PASSWORD = "TibiTibi3022"
REMOTE_PRIVATE_KEY_PATH = PROJECT_DIR + '//..//new_key'
REMOTE_SERVER_COMMAND_PATH = "/home/" + REMOTE_USER + "/style_transform_face_detection/src/server/run_style_transfer.py"

# ############## Style Transfer Globals ##############
NAAS_ALG = 0  # NAAS algorithm used in neural_algorithm_Artistic_sty
STYLE_ONLY = 1  # Style_only algorithm used in StyleTransformer
PRE_TRAINED_NAAS = 2  # Fast transfer
alg_index = PRE_TRAINED_NAAS

DEFAULT_TRANSFER_ITERATIONS = 200
SHOW_PROGRESS = 1
DEFAULT_PROGRESS_UPDATE_INTERVAL = 10

DEBUG_TRANSFORM = True

# Debug Flags:
# Note: the next flags will present different data about all the faces in one window each, meaning those flags wont
# interrupt when you want to see all the progress in general
DEBUG_SHOW_GRAY_IMAGE_MODE = False  # Able to see the gray image in the face detection process
DEBUG_SHOW_CLOSED_IMAGE_MODE = False  # Able to see the closed image
DEBUG_SHOW_VALLY_FIELD_MODE = False  # Able to see the vally field of the image
DEBUG_SHOW_ALL_POSSIBLE_EYES_MODE = False  # Able to see all the possible eyes in the image
DEBUG_SHOW_BEST_POSSIBLE_EYES_MODE = False  # Able to see the best possible eyes in the image
DEBUG_SHOW_EYE_COORDINATES = False  # Able to see the coordinates of the possible eyes
DEBUG_SHOW_FINAL_RESULTS = False  # Able to see the final result of the face detection stage

# Note: the next flags will present different data about each faces in new window, meaning those flags might
# interrupt when you want to see all the progress in general
DEBUG_SHOW_RESCALED_IMAGE = False  # Able to see the rescaled image of each possible face
DEBUG_SHOW_AFTER_ROTATE_MODE = False  # Able to see each possible face after rotating it so the eye will be straight
DEBUG_SHOW_SYMMETRY_FACES = False  # Able to see all the symmetrical possible faces
DEBUG_SHOW_PROJECTION_MODE = False  # Able to see the projections (x and y) for each possible face

DEBUG_SKIP_FACE_SWITCH = False
DEBUG_REGRESSION_MODE = False
DEBUG_INDEX_FACE = False
DEBUG_INDEX = []
DEBUG_INFO_MODE = False  # Able to see information of the face detection process

fast_trans_image_name = "scream"


def path_to_resized_tk_image(path_to_image, force_max_width=256, face_image = False):
    """
    Get path to image and max width of the wanted image, return its tk image of the requested size
    :param path_to_image: The path
    :param force_max_width: The wanted size of the image
    :return: Tk image of the requested size
    """
    # TODO: Fix so that the returned image, in the given size will contain the whole image

    try:
        img = Image.open(path_to_image)
    except IOError:  # TODO: I don't know why but the popup msg comes only in debug mode, fix that
        popup_msg("ERROR", "Could not Open Image:" + path_to_image + " \n " + str(sys.exc_info()))
        return Image.new("RGB", (250, 250), 0)
    if face_image:
        resized_image = resize_with_pad_keep_ratio(img, force_max_width, force_max_width)
    else:
        resized_image = pil_resize_image(img, force_max_width)
    return_img = ImageTk.PhotoImage(resized_image)
    return return_img


def pil_image_to_resized_tk_image(img):
    """
    Transfer pil image to tk image
    :param img: Pil image to transfer
    :return: Tk image of the given image
    """

    max_dim = IMAGE_MIN_SIZE
    long = max(img.size)
    if max_dim > long:
        return ImageTk.PhotoImage(img)
    scale = max_dim / long
    # TODO: Decide on image size depending on algorithm.
    # Make sure size is a global variable and not hard coded numbers in thins function.
    resized_image = img.resize((round(img.size[0] * scale), round(img.size[1] * scale)), Image.ANTIALIAS)
    return ImageTk.PhotoImage(resized_image)


def pil_resize_image(img, force_max_width=256):
    """
    Resize given pil image to given size
    :param img: Pil image
    :param force_max_width: Wanted size
    :return: Resized pil image
    """
    ratio = img.size[1]/img.size[0]
    if img.size[0] >= img.size[1]:
        ret = img.resize((force_max_width, round(force_max_width * ratio)), Image.ANTIALIAS)
    else:
        ret = img.resize((round(force_max_width * (1/ratio)), force_max_width), Image.ANTIALIAS)
    return ret


def popup_msg(title, msg):
    """
    Show popup msg
    :param title: Title for the popup msg
    :param msg: Msg to show on the popup msg
    """

    popup = Tk()
    popup.wm_title(title)
    label = ttk.Label(popup, text=msg, font=12)
    label.pack(side="top", fill="x", pady=10)
    ok_button = ttk.Button(popup, text="Okay", command=popup.destroy)
    ok_button.pack()
    return


def represents_int(value):
    """
    Checks if parameter 'value' can be converted to integer
    :param value: any kind of object
    :return: Boolean. True if 'value' can be converted to integer
    """
    try:
        int(value)
        return True
    except ValueError:
        return False


def subprocess_cmd(command):
    process = subprocess.Popen(command, stderr=subprocess.PIPE, shell=True)
    proc_stderr = process.communicate()[1].strip()
    return proc_stderr


def connect_to_sftp_server(host=HOST, port=REMOTE_PORT, username=REMOTE_USER, password='',
                           private_key=REMOTE_PRIVATE_KEY_PATH, private_key_pass=REMOTE_PRIVATE_KEY_PASSWORD, test=False):
    """
    Connect to server
    :param host: Host adders
    :param port: Port number
    :param username: Username of the remote server
    :param password: Password of thr remote server
    :param private_key: # TODO: fill those
    :param private_key_pass: # TODO: fill those
    :return: In case of success return the sftp, else return None TODO: Explain what is th sftp
    """
    try:
        if isinstance(port, str):
            port = int(port)
        cnopts = pysftp.CnOpts()
        cnopts.hostkeys = None
        if password == '':
            password = None
        sftp = pysftp.Connection(host, username=username, port=port, password=password,
                                 private_key=private_key, private_key_pass=private_key_pass, cnopts=cnopts)
        sftp.chdir('public')
        return sftp
    except (paramiko.ssh_exception.SSHException , FileNotFoundError, pysftp.exceptions.ConnectionException) as error:
        if test:
            return str(sys.exc_info()[1])
        popup_msg("ERROR", str(sys.exc_info()[1]))
    return None


def get_r_portable(sftp, remote_dir, local_dir, preserve_mtime=False):
    """
    # TODO: fill
    :param sftp: # TODO: fill
    :param remote_dir: # TODO: fill
    :param local_dir: # TODO: fill
    :param preserve_mtime: # TODO: fill
    """
    for entry in sftp.listdir(remote_dir):
        remote_path = remote_dir + "/" + entry
        local_path = os.path.join(local_dir, entry)
        mode = sftp.stat(remote_path).st_mode
        if S_ISDIR(mode):
            try:
                os.mkdir(local_path)
            except OSError:  # The directory already exists
                pass
            get_r_portable(sftp, remote_path, local_path, preserve_mtime)
        elif S_ISREG(mode):
            sftp.get(remote_path, local_path, preserve_mtime=preserve_mtime)
        return


def show_image(image_to_present, title, display_length=IMAGE_EDGE_LENGTH_DISPLAY, is_arr=False,
               regression_candidate=False):
    """
    Display image in given length with given title
    :param image_to_present: Pil image or arr represent an image
    :param title: A title for the displaying
    :param display_length: The length of the area on the display window
    :param is_arr: False- the given image is pil image, True- the given image is arr represent an image
    """
    if is_arr:
        pil_image_to_present = Image.fromarray(image_to_present).convert("L")
    else:
        pil_image_to_present = image_to_present
    width_percent = (display_length / float(pil_image_to_present.size[0]))
    height_size = int((float(pil_image_to_present.size[1]) * float(width_percent)))
    resized_pil_image_to_present = pil_image_to_present.resize((display_length, height_size), Image.ANTIALIAS)

    arr_of_image = np.array(resized_pil_image_to_present)
    cv2.imshow(title, arr_of_image)
    key = cv2.waitKey()
    if key == 121 and regression_candidate:  # 121 is 'y' char value i think
        pil_image_to_present.save(EYES_DIR + title + ".jpg", "JPEG")
        cv2.destroyAllWindows()
        return True
    cv2.destroyAllWindows()
    plt.close()
    return False


def filter_faces_from_result(result_pil_image, content_image, faces):
    draw_og_image = ImageDraw.Draw(result_pil_image)
    if content_image.size != result_pil_image.size:
        content_image = resize_with_pad_keep_ratio(content_image, result_pil_image.size[0], result_pil_image.size[1])
    for f in faces:
        print(f)
        polygon, angle, _ = f

        #upper_left, (y1, x1), lower_right, (y2, x2) = polygon
        p0, p1, p2, p3 = polygon
        polygon = Polygon([p0, p1, p2, p3, p0])
        p0 = new_position_after_resize(p0, 128, 120, result_pil_image.size[0], result_pil_image.size[1])
        p2 = new_position_after_resize(p2, 128, 120, result_pil_image.size[0], result_pil_image.size[1])
        y1, x1 = p0
        y2, x2 = p2
        smallest_buffer = int((x2-x1)*0.1)
        largest_buffer = int((x2-x1)*0.3)

        buff_diff = largest_buffer-smallest_buffer
        if buff_diff == 0:
            # make sure not to divide by zero
            continue
        step = max(int(buff_diff/5), 1)
        print("Running loop on rect " + str(polygon) + " (i=" + str(smallest_buffer) + "; i<" + str(largest_buffer+1) +
              "; i+=" + str(step) + " )")
        debug = False
        if debug:
            draw_og_image.ellipse((p0, p2), outline="red", width=5)
        for buffer in range(smallest_buffer, largest_buffer+1, step):
            tx1 = max(0, x1-buffer)
            tx2 = min(result_pil_image.size[0], x2 + buffer)
            ty1 = max(0, int(y1-buffer*1.1))
            ty2 = min(result_pil_image.size[1], int(y2+buffer*1.1))
            bloated_rect = (tx1, ty1, tx2, ty2)
            mask = Image.new("RGBA", result_pil_image.size, (0, 0, 0, 0))
            temp_content_image = content_image.crop(bloated_rect)
            mask = mask.crop(bloated_rect)
            draw = ImageDraw.Draw(mask)
            alpha = int(255*((buff_diff-(buffer-smallest_buffer))/buff_diff))
            draw.ellipse((0, 0) + mask.size, fill=(0, 0, 0, alpha))
            del draw
            result_pil_image.paste(temp_content_image, box=(tx1, ty1), mask=mask)
    return result_pil_image


def show_image_with_plot(image_to_present, title="", is_arr=False, coordinates=None):
    """
    Display image in given length with given title by plot screen able to combine a plot with an image
    :param image_to_present: Pil image or arr represent an image
    :param title: a title of the plot
    :param is_arr: False- the given image is pil image, True- the given image is arr represent an image
    :param coordinates: list of coordinates that the plot need to cool in black
    """
    if is_arr:
        image_to_present = Image.fromarray(image_to_present).convert("L")

    plt.imshow(image_to_present, cmap='gray')
    plt.draw()
    plt.suptitle(title, fontsize=9)
    if coordinates is not None:
        coordinates_count = 0
        for coordinate in coordinates:
            coordinates_count += 1
            x = coordinate[0]
            y = coordinate[1]
            if DEBUG_SHOW_EYE_COORDINATES:
                plt.annotate("(" + str(y) + "," + str(x) + ")", (y, x))
    try:
        plt.pause(0.001)
        plt.waitforbuttonpress()
        plt.close()
    except Exception:
        print(Exception)
    return


def coordinates_list_to_arr(coordinates, n, m, arr=None, buffer=None):
    """
    Set value of 1 in specific cells in matrix arr according to list of coordinates of the cells
    :param coordinates: List of coordinates
    :param n: Number of rows
    :param m: Number of columns
    :param arr: Optimal-  give the option to change the given arr only in the given coordinates to 1
    :return: Arr of with the value 1 in the given coordinates
    """
    if arr is None:
        arr = np.zeros((n, m))
    for coordinate in coordinates:
        x = coordinate[0]
        y = coordinate[1]
        if buffer is not None:
            arr[x-buffer:x+buffer+1, y-buffer:y+buffer+1] = 255
        arr[x][y] = 1
    return arr


def present_coordinates(data_matrix, title, image):
    # Present a matrix of data as image
    temp_image = image.copy()
    temp_image.setflags(write=1)
    arr_of_image_with_marked_eyes = coordinates_list_to_arr(data_matrix, len(image), len(image[0]), arr=temp_image,
                                                            buffer=int(max(image.shape)*1/100))
    show_image_with_plot(arr_of_image_with_marked_eyes, title, is_arr=True, coordinates=data_matrix)


def flood_fill(matrix, x, y, eyes_list):
    if matrix[x][y] == 1:
        eyes_list.append((x, y))
        matrix[x][y] = 0

        # Recursively invoke flood fill on all surrounding cells:
        if x > 0:
            flood_fill(matrix, x - 1, y, eyes_list)
        if x < len(matrix) - 1:
            flood_fill(matrix, x + 1, y, eyes_list)
        if y > 0:
            flood_fill(matrix, x, y - 1, eyes_list)
        if y < len(matrix[0]) - 1:
            flood_fill(matrix, x, y + 1, eyes_list)
    return eyes_list


def vally_field(image):
    """
    Calculate the vally field (the areas that have a lot of changes) of the given image
    by doing closing operator of size (CLOSING_OPERATOR_SIZE, CLOSING_OPERATOR_SIZE)
    :param image: Pil image to calc vally field on it
    :return: The vally field  of given image
    """

    # Closing operation - removes small holes
    image_after_close = ndimage.grey_closing(image, size=(CLOSING_OPERATOR_SIZE, CLOSING_OPERATOR_SIZE))
    if DEBUG_SHOW_CLOSED_IMAGE_MODE:
        show_image(image_after_close, 'Image after closing', is_arr=True)

    result = image_after_close - image  # vally field = f(x, y) * B  - f(x,y)
    if DEBUG_SHOW_VALLY_FIELD_MODE:
        show_image(result, 'Vally field', is_arr=True)

    return result


def average_neighbors_values_in_x(grayscale_image_arr, i, j, level_degree, edge_length):
    # Calculate the average values of the neighbors of given pixel in the x direction in distance of level_degree
    # if one of the neighbors does not exists returns -1

    if i - level_degree > -1 and i + level_degree < edge_length:
        gray_sum = int(grayscale_image_arr[i - level_degree][j]) + int(grayscale_image_arr[i + level_degree][j])
        return gray_sum / 2
    else:
        return -1


def average_value_around_coordinate(array, x, y, size):
    # Return the average grayscale value in the square in size - (size X size) around pixel (x,y) in the given array
    # if the square exceeding the border returns -1

    sum_of_scope = 0
    # Check if the pixel can be surrounded by square in required size
    if x - (size - 1) / 2 > 0 and x + (size - 1) / 2 < len(array) and y - (size - 1) / 2 > 0 and y + (
            size - 1) / 2 < len(array[0]):
        # Move to the upper left point in the square
        x -= int((size - 1) / 2)
        y -= int((size - 1) / 2)

        # Sum the values in the square
        for i in range(0, size):
            for j in range(0, size):
                sum_of_scope += array[x + i][y + j]

        return sum_of_scope / (size * size)

    else:
        return -1


def new_position_after_resize(current_point, current_width, current_height, new_width, new_height):
    new_x = int((current_point[0] / current_width) * new_width)
    new_y = int((current_point[1] / current_height) * new_height)
    return new_x, new_y


def new_position_after_rotate(old_x0, old_y0, angle, edge_length_x, edge_length_y):
    old_x0 -= int(edge_length_x / 2)
    old_y0 -= int(edge_length_y / 2)

    angle = math.radians(angle)
    new_x0 = old_y0 * math.sin(angle) + old_x0 * math.cos(angle)
    new_y0 = old_y0 * math.cos(angle) - old_x0 * math.sin(angle)

    new_x0 += edge_length_x / 2
    new_y0 += edge_length_y / 2

    return int(new_x0), int(new_y0)


def arr_symmetry(arr_to_check_symmetry):
    sum_of_difference = 0

    for i in range(0, len(arr_to_check_symmetry)):
        for j in range(0, int(len(arr_to_check_symmetry[0]) / 2)):
            sum_of_difference += \
                abs(arr_to_check_symmetry[i][j] -
                    arr_to_check_symmetry[i][len(arr_to_check_symmetry[0]) -
                                             (j + 1)])

    return sum_of_difference / (len(arr_to_check_symmetry) * int(len(arr_to_check_symmetry[0]) / 2))


def local_minimums(y, distance=3, prominence=7):
    end_min = 0
    start_min = 0

    minimums, min_properties = find_peaks(-y, distance=distance, prominence=(prominence, 255))

    if y[len(y) - 1] < y[len(y) - 2]:
        end_min = 1
    if y[0] < y[1]:
        start_min = 1

    return minimums.size, end_min, start_min, minimums, min_properties


def fix_lighting(data):
    # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_histograms/py_histogram_equalization/py_histogram_equalization.html
    # "Histograms Equalization in OpenCV"
    # equ = cv2.equalizeHist(data)
    # Image.fromarray(equ).show()
    # Image.fromarray(data).show()

    # https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_histograms/py_histogram_equalization/py_histogram_equalization.html
    # CLAHE - The last one
    # clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    # cl1 = clahe.apply(data)
    # Image.fromarray(cl1).show()
    # Image.fromarray(data).show()

    data_equalized = image_histogram_equalization(data)[0]
    # Image.fromarray(data_equalized).show()
    # Image.fromarray(data).show()
    return data_equalized


def image_histogram_equalization(image, number_bins=256):
    # from http://www.janeriksolem.net/2009/06/histogram-equalization-with-python-and.html

    # get image histogram
    image_histogram, bins = np.histogram(image.flatten(), number_bins, density=True)
    cdf = image_histogram.cumsum()  # cumulative distribution function
    cdf = 255 * cdf / cdf[-1]  # normalize

    # use linear interpolation of cdf to find new pixel values
    image_equalized = np.interp(image.flatten(), bins[:-1], cdf)

    return image_equalized.reshape(image.shape), cdf


def adjust_gamma(image, gamma=1.0):
    inv_gamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** inv_gamma) * 255
                      for i in np.arange(0, 256)]).astype("uint8")
    return cv2.LUT(image, table)


def y_projection(arr):
    y = arr.mean(axis=1)
    y = y.astype(int)
    return y


def x_projection(arr):
    x = arr.mean(axis=0)
    x = x.astype(int)
    return x


def show_plot_function(x, is_x_projection=True, title="Plot", peaks=None):
    if is_x_projection:
        plt.title("X - projection" + title)
        plt.xlabel('x')
    else:
        plt.title("Y - projection " + title)
        plt.xlabel("y")

    plt.ylabel("gray-level")
    plt.plot(list(range(0, len(x))), x)
    if peaks is not None:
        plt.plot(peaks, x[peaks], "x")
    plt.show(block=False)
    plt.pause(0.001)


def mark_square_on_image(image, square_start_point, square_end_point, square_angle,
                         points=None, is_arr=False, title="Result"):
    if is_arr:
        image = Image.fromarray(image)

    image = image.rotate(square_angle)
    buffer = int(max(image.size)*1/100)
    image = np.asarray(image)
    image.setflags(write=True)

    if points is not None:
        for point in points:
            image[point[1]-buffer:point[1]+buffer+1, point[0]-buffer:point[0]+buffer+1] = 0
            image[point[1]][point[0]] = 255

    for i in range(square_start_point[1], square_end_point[1]):
        for k in range(0, 1):
            image[i][square_start_point[0]+k] = 1

    for i in range(square_start_point[1], square_end_point[1]):
        for k in range(0, 1):
            image[i][square_end_point[0]+k] = 1

    for i in range(square_start_point[0], square_end_point[0]):
        for k in range(0, 1):
            image[square_start_point[1]+k][i] = 1

    for i in range(square_start_point[0], square_end_point[0]):
        for k in range(0, 1):
            image[square_end_point[1]+k][i] = 1

    show_image_with_plot(image, title=title, is_arr=True)
    return
    # return image


def resize_with_pad_keep_ratio(image, edge_length_x, edge_length_y):
    old_size = image.size  # old_size[0] is in (width, height) format

    if old_size[0] > old_size[1]:
        desired_size = edge_length_x
    else:
        desired_size = edge_length_y

    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])

    image = image.resize(new_size, Image.ANTIALIAS)
    new_im = Image.new("RGB", (edge_length_x, edge_length_y))
    new_im.paste(image, ((desired_size - new_size[0]) // 2,
                         (desired_size - new_size[1]) // 2))
    return new_im
