import os

from PIL import Image

from src import utils
from src.face_detection import run_face_detection
from src.face_detection.shared_variables_face_detection import EDGE_LENGTH_X, EDGE_LENGTH_Y


def run_training():
    utils.DEBUG_SHOW_BEST_POSSIBLE_EYES_MODE = True
    utils.DEBUG_SHOW_AFTER_ROTATE_MODE = True
    already_trained = {}
    for file in os.listdir(utils.EYES_DIR):
        already_trained[file.split(",")[0]] = 1

    for face_image in os.listdir(utils.FACES_DIR):
        if face_image.split(".")[0] in already_trained.keys():
            continue
        print(face_image)
        img = Image.open(str(utils.FACES_DIR) + "/" + str(face_image))
        run_face_detection.detect_faces(img, face_image.split(".")[0])
        print("\n")


def run_regression():
    """
    Run face detector on a folder (utils.FACES_DIR) contain images of faces,
    and present the face recognition percentages according to indexes of faces that were manually set
    """
    successful_total = 0
    unsuccessful_total = 0
    file_name_index_dict = {}

    # Save the eyes coordinates and their index in dictionary
    index_eye_coordinates = os.listdir(utils.EYES_DIR)
    for file_name in index_eye_coordinates:
        file_name = file_name.split(".")[0]
        splitted_file_name = file_name.split(",")
        key = splitted_file_name[0]
        index = splitted_file_name[1]
        file_name_index_dict[key] = int(index)

    # Run face detection on each face image
    for face_image in os.listdir(utils.FACES_DIR):
        image_name = face_image.split(".")[0]
        if image_name not in file_name_index_dict.keys():
            continue

        utils.DEBUG_INDEX = [file_name_index_dict[image_name]]
        unsuccessful = 0
        successful = 0
        img = Image.open(str(utils.FACES_DIR) + "/" + str(face_image))

        image_for_detection = utils.resize_with_pad_keep_ratio(img, EDGE_LENGTH_X, EDGE_LENGTH_Y)
        faces = run_face_detection.detect_faces(image_for_detection, image_name)

        symmetry_value = "Wanted Face Not Detected"
        for face in faces:
            _, _, face_obj = face
            if file_name_index_dict[image_name] == face_obj.index:
                successful += 1
                successful_total += 1
                symmetry_value = str(face_obj.symmetry)
            else:
                unsuccessful += 1
                unsuccessful_total += 1
        print("image:" + face_image + " detected face:" + str(successful) +
              " detected wrong faces:" + str(unsuccessful) + "  |  symmetry: " + symmetry_value)

    # Show result of the scan on the folder
    print("COMPLETED with " + str(successful_total) + "/" + str(len(index_eye_coordinates)) +
          " (" + str(100*successful_total/len(index_eye_coordinates)) + "%)")
    print("right / wrong " + str(successful_total) + "/" + str(unsuccessful_total))


run_regression()
# run_training()
