import numpy as np
from PIL import Image
from matplotlib import pyplot as plt
from shapely.geometry import Polygon
import src.utils as utils
from src.face_detection import run_face_detection as Face
from src.face_detection.shared_variables_face_detection import EDGE_LENGTH_X, EDGE_LENGTH_Y


def test_detect_faces():
    img_name = "Omri"
    path = utils.CONTENTS_DIR + img_name + ".jpg"
    img = Image.open(path)

    image_for_detection = utils.resize_with_pad_keep_ratio(img, EDGE_LENGTH_X, EDGE_LENGTH_Y)
    info = Face.detect_faces(image_for_detection, img_name)
    print(info)
    polygon,_ , face = info[0]

    wanted_size = (800, 760)
    p0 = utils.new_position_after_resize((p0), image_for_detection.size[1], image_for_detection.size[0],
                                         wanted_size[1], wanted_size[0])
    p1 = utils.new_position_after_resize(p1, image_for_detection.size[1], image_for_detection.size[0],
                                         wanted_size[1], wanted_size[0])
    image_for_detection = image_for_detection.resize(wanted_size)
    utils.present_coordinates([p0, p1], "Result eyes- after resize", np.asarray(image_for_detection))


def show_poly_on_plot(list_of_xy):
    fig = plt.figure(1, figsize=(5, 5), dpi=90)
    ax = fig.add_subplot(111)
    min_x = int(min([min(x) for x, y in list_of_xy]))
    max_x = int(max([max(x) for x, y in list_of_xy]))
    min_y = int(min([min(y) for x, y in list_of_xy]))
    max_y = int(max([max(y) for x, y in list_of_xy]))
    padding_x = int((max_x - min_x)/10 + 1)
    padding_y = int((max_y - min_y)/10 + 1)
    for x, y in list_of_xy:
        ax.plot(x, y)

    ax.set_title('Polygon Edges')

    xrange = [min_x-padding_x, max_x+padding_x]
    yrange = [min_y-padding_y, max_y+padding_y]
    ax.set_xlim(*xrange)
    ax.set_xticks(list(range(*xrange)) + [xrange[-1]])
    ax.set_ylim(*yrange)
    ax.set_yticks(list(range(*yrange)) + [yrange[-1]])
    plt.pause(0.001)
    plt.waitforbuttonpress()
    plt.close()
    print("Done")


# poly = Polygon([(1, 1), (2, 2), (3, 1),
#                (2, 0), (1, 1)])
# x1, y1 = poly.exterior.xy
#
# poly2 = Polygon([(0, 0), (0, 2), (1, 1),
#                (2, 2), (2, 0), (1, 0.8), (0, 0)])
# x2,y2 = poly2.exterior.xy
#
# new_poly = poly.intersection(poly2)
# show_poly_on_plot([poly.exterior.xy, poly2.exterior.xy, new_poly.exterior.xy])

test_detect_faces()
