import src.graphics.main_window as mw

import src.utils as utils

if __name__ == "__main__":
    utils.MAIN_WINDOW = mw.MainWindowGui()
    utils.MAIN_WINDOW.top.mainloop()
